#include <cstring>
#include <doctest/doctest.h>
#include <nlohmann/json.hpp>

#include "iothub_message.h"

#include "nomad/exceptions.hxx"
#include "nomad/message.hxx"

/*
   Telemetry message property used to indicate the message's component.
   */
static const char *g_pnp_telemetry_component_property = "$.sub";
/*
   Default encoding for messages.
   */
static const char *g_default_content_encoding = "utf-8";
/*
   Default content type for messages.
   */
static const char *g_default_content_type = "application%2fjson";

nomad::Message::Message(Message &&m) {
  message = m.message;
  m.message = nullptr;
}

nomad::Message::Message(const nlohmann::json &data) {
  if (!(message = IoTHubMessage_CreateFromString(data.dump().c_str()))) {
    throw Message_Create_Error{};
  }
  if (auto res = IoTHubMessage_SetContentEncodingSystemProperty(
          message, g_default_content_encoding);
      res != IOTHUB_MESSAGE_OK) {
    throw Property_Set_Error{"Failed to set content encoding.", res};
  }
  if (auto res = IoTHubMessage_SetContentTypeSystemProperty(
          message, g_default_content_type);
      res != IOTHUB_MESSAGE_OK) {
    throw Property_Set_Error{"Failed to set content type.", res};
  }
}

nomad::Message::Message(nlohmann::json &&data) : Message{data} {}

nomad::Message::Message(const std::string &name, const nlohmann::json &content)
    : Message{nlohmann::json{{name, content}}} {}

nomad::Message::Message(const std::string &name, nlohmann::json &&content)
    : Message(name, content) {}

TEST_CASE("Constructor from const lvalue json.") {
  nlohmann::json data{{"key", 1}};
  SUBCASE("From lvalue json.") { CHECK_NOTHROW(nomad::Message{data}); }
  SUBCASE("From rvalue json.") {
    CHECK_NOTHROW(nomad::Message{std::move(data)});
  }
  SUBCASE("From name and lvalue json.") {
    CHECK_NOTHROW(nomad::Message{"name", data});
  }
  SUBCASE("From name and rvalue json.") {
    CHECK_NOTHROW(nomad::Message{"name", std::move(data)});
  }
}

nomad::Message &nomad::Message::operator=(Message &&m) {
  message = m.message;
  m.message = nullptr;
  return *this;
}

void nomad::Message::set_message_id(const std::string &id) {

  auto result = IoTHubMessage_SetMessageId(message, id.c_str());

  if (result != IOTHUB_MESSAGE_OK) {
    throw Property_Set_Error{g_pnp_telemetry_component_property, result};
  }
}

void nomad::Message::set_correlation_id(const std::string &id) {

  auto result = IoTHubMessage_SetCorrelationId(message, id.c_str());

  if (result != IOTHUB_MESSAGE_OK) {
    throw Property_Set_Error{g_pnp_telemetry_component_property, result};
  }
}

void nomad::Message::set_component(const std::string &component_name) {
  /*
     If the component will be used, then specify this as a property of the
     message.
     */
  auto result = IoTHubMessage_SetProperty(
      message, g_pnp_telemetry_component_property, component_name.c_str());

  if (result != IOTHUB_MESSAGE_OK) {
    throw Property_Set_Error{g_pnp_telemetry_component_property, result};
  }
}

TEST_CASE("Setting properties.") {
  nomad::Message m{nlohmann::json{{"field", 1}}};
  SUBCASE("Message ID") { CHECK_NOTHROW(m.set_message_id("unique_id")); }
  SUBCASE("Correlation ID") {
    CHECK_NOTHROW(m.set_correlation_id("correlation_id"));
  }
  SUBCASE("Component") { CHECK_NOTHROW(m.set_component("component")); }
}

bool nomad::Message::is_valid() const { return message != nullptr; }

nomad::IoT_Hub_Message *nomad::Message::get_handle() const { return message; }

nomad::Message::~Message() { IoTHubMessage_Destroy(message); }
