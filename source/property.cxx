#include <doctest/doctest.h>
#include <doctest/trompeloeil.hpp>

#include "nomad/component.hxx"
#include "nomad/property.hxx"

const char *g_success_description = "success";
const char *g_failure_description = "failure";

/*
  "ac" is a numeric field used for reportin status codes.
   */
static const char *g_status_key = "ac";

/*
  "ad" is an optional description field.
   */
static const char *g_description_key = "ad";

/*
   "av" is the version number sent to the device.
   */
static const char *g_version_key = "av";

/*
   "value" is the field for the property's actual value.
   */
static const char *g_value_key = "value";

/*
   Serializes a read only property according to the component's name.

   May throw: nlohmann::detail::type_error
   */
std::string nomad::serialize_read_only_property(const std::string &name,
                                                const nlohmann::json &content,
                                                const std::string &component) {

  /*
     IoTHub adds a JSON field "__t":"c" into desired top-level JSON objects that
     represent components.  Without this marking, the object is treated as a
     property off the root component.
     */
  if (component != nomad::default_component_name) {
    return nlohmann::json{
        {component, {{nomad::component_marker, "c"}, {name, content}}}}
        .dump();
  }
  return nlohmann::json{{name, content}}.dump();
}

SCENARIO("Serializing a read only property for the default component.") {

  GIVEN("The component is the default one.") {

    WHEN("The content is an integer value") {

      THEN("The result should be a simple key-value json.") {

        CHECK_EQ(nomad::serialize_read_only_property(
                     "total_storage", 10000, nomad::default_component_name),
                 R"({"total_storage":10000})");
      }
    }
    WHEN("The content is a double value") {

      THEN("The result should be a simple key-value json.") {

        CHECK_EQ(nomad::serialize_read_only_property(
                     "cpu_temperature", 36.5, nomad::default_component_name),
                 R"({"cpu_temperature":36.5})");
      }
    }
    WHEN("The content is a string value") {

      THEN("The result should be a simple key-value json.") {

        CHECK_EQ(
            nomad::serialize_read_only_property("cpu_architecture", "x86_64",
                                                nomad::default_component_name),
            R"({"cpu_architecture":"x86_64"})");
      }
    }
  }
}

SCENARIO("Serializing a read only property for a regular component.") {

  GIVEN("The component is not the default one.") {

    WHEN("The content is an integer value") {

      THEN("The result should be wrapped in the component name.") {

        CHECK_EQ(nomad::serialize_read_only_property("total_storage", 10000,
                                                     "monitoring"),
                 R"({"monitoring":{"__t":"c","total_storage":10000}})");
      }
    }
    WHEN("The content is a double value") {

      THEN("The result should be wrapped in the component name.") {

        CHECK_EQ(nomad::serialize_read_only_property("cpu_temperature", 36.5,
                                                     "monitoring"),
                 R"({"monitoring":{"__t":"c","cpu_temperature":36.5}})");
      }
    }
    WHEN("The content is a string value") {

      THEN("The result should be wrapped in the component name.") {
        CHECK_EQ(nomad::serialize_read_only_property("cpu_architecture",
                                                     "x86_64", "monitoring"),
                 R"({"monitoring":{"__t":"c","cpu_architecture":"x86_64"}})");
      }
    }
  }
}

/*
   Serializes writable properties according to the component's name.

   May throw: nlohmann::detail::type_error
   */
static std::string serialize_writable_property(const std::string &name,
                                               const nlohmann::json &content,
                                               const std::string &component,
                                               const nomad::Status status,
                                               const std::string &description,
                                               const int version) {

  /*
     IoTHub adds a JSON field "__t":"c" into desired top-level JSON objects that
     represent components.  Without this marking, the object is treated as a
     property off the root component.
     */
  if (component != nomad::default_component_name) {
    return nlohmann::json{{component,
                           {{nomad::component_marker, "c"},
                            {name,
                             {{g_status_key, status},
                              {g_description_key, description},
                              {g_version_key, version},
                              {g_value_key, content}}}}}}
        .dump();
  }
  return nlohmann::json{{name,
                         {{g_status_key, status},
                          {g_description_key, description},
                          {g_version_key, version},
                          {g_value_key, content}}}}
      .dump();
}

SCENARIO("Serializing a writable property for the default component.") {

  GIVEN("The component is the default one.") {

    WHEN("The content is an integer value") {

      THEN("The result should contain the ac, av fields and optionally the "
           "ad.") {

        CHECK_EQ(serialize_writable_property(
                     "max_count", 5, nomad::default_component_name,
                     nomad::STATUS_SUCCESS, "success", 1),
                 R"({"max_count":{"ac":200,"ad":"success","av":1,"value":5}})");
      }
    }
    WHEN("The content is a double value") {

      THEN("The result should contain the ac, av fields and optionally the "
           "ad.") {

        CHECK_EQ(
            serialize_writable_property("max_temperature", 90.7,
                                        nomad::default_component_name,
                                        nomad::STATUS_SUCCESS, "success", 1),
            R"({"max_temperature":{"ac":200,"ad":"success","av":1,"value":90.7}})");
      }
    }
    WHEN("The content is a string value") {

      THEN("The result should contain the ac, av fields and optionally the "
           "ad.") {

        CHECK_EQ(
            serialize_writable_property("model_id", "peoplenet_v2_pruned.etlt",
                                        nomad::default_component_name,
                                        nomad::STATUS_SUCCESS, "success", 1),
            R"({"model_id":{"ac":200,"ad":"success","av":1,"value":"peoplenet_v2_pruned.etlt"}})");
      }
    }
  }
}

SCENARIO("Serializing a writable property for a regular component.") {

  GIVEN("The component is not the default one.") {

    WHEN("The content is an integer value") {

      THEN("The result should wrap the property in the component name and "
           "contain the ac, av fields and optionally the ad.") {

        CHECK_EQ(
            serialize_writable_property("max_count", 5, "monitoring",
                                        nomad::STATUS_SUCCESS, "success", 1),
            R"({"monitoring":{"__t":"c","max_count":{"ac":200,"ad":"success","av":1,"value":5}}})");
      }
    }
    WHEN("The content is a double value") {

      THEN("The result should wrap the property in the component name and "
           "contain the ac, av fields and optionally the ad.") {

        CHECK_EQ(
            serialize_writable_property("max_temperature", 90.7, "monitoring",
                                        nomad::STATUS_SUCCESS, "success", 1),
            R"({"monitoring":{"__t":"c","max_temperature":{"ac":200,"ad":"success","av":1,"value":90.7}}})");
      }
    }
    WHEN("The content is a string value") {

      THEN("The result should wrap the property in the component name and "
           "contain the ac, av fields and optionally the ad.") {

        CHECK_EQ(
            serialize_writable_property("model_id", "peoplenet_v2_pruned.etlt",
                                        "monitoring", nomad::STATUS_SUCCESS,
                                        "success", 1),
            R"({"monitoring":{"__t":"c","model_id":{"ac":200,"ad":"success","av":1,"value":"peoplenet_v2_pruned.etlt"}}})");
      }
    }
  }
}

nomad::Read_Only_Property::Read_Only_Property(
    const std::string &name,
    const std::shared_ptr<IoT_Hub_Client_Adapter> &client)
    : property_name{name}, iot_hub_client{std::move(client)} {}

bool nomad::Read_Only_Property::send(const std::string &component) const {
  return iot_hub_client->send_property(serialize_read_only_property(
      property_name, get_property_content(), component));
}

const std::string &nomad::Read_Only_Property::get_name() const {
  return property_name;
}

nomad::Writable_Property::Writable_Property(Writable_Property &&property)
    : property_name{std::move(property.property_name)},
      property_content(std::move(property.property_content)),
      iot_hub_client{property.iot_hub_client} {}

nomad::Writable_Property::Writable_Property(
    const std::string &name, const nlohmann::json &content,
    const std::shared_ptr<IoT_Hub_Client_Adapter> &client)
    : property_name{name},
      property_content(content), iot_hub_client{std::move(client)} {}

nomad::Writable_Property &
nomad::Writable_Property::operator=(Writable_Property &&property) {
  property_name = std::move(property.property_name);
  property_content = std::move(property.property_content);
  iot_hub_client = property.iot_hub_client;
  return *this;
}

void nomad::Writable_Property::set_content(const nlohmann::json &content) {
  property_content = content;
}

const nlohmann::json &nomad::Writable_Property::get_content() const {
  return property_content;
}

bool nomad::Writable_Property::send(const std::string &component,
                                    const Status status,
                                    const std::string &description,
                                    const int version) const {
  return iot_hub_client->send_property(
      serialize_writable_property(property_name, property_content, component,
                                  status, description, version));
}

nomad::Persistent_Property::Persistent_Property(Persistent_Property &&property)
    : property_name{std::move(property.property_name)},
      property_content(std::move(property.property_content)),
      iot_hub_client{property.iot_hub_client} {}

nomad::Persistent_Property::Persistent_Property(
    const std::string &name, const nlohmann::json &content,
    const std::shared_ptr<IoT_Hub_Client_Adapter> &client)
    : property_name{name},
      property_content(content), iot_hub_client{std::move(client)} {}

nomad::Persistent_Property &
nomad::Persistent_Property::operator=(Persistent_Property &&property) {
  property_name = std::move(property.property_name);
  property_content = std::move(property.property_content);
  iot_hub_client = property.iot_hub_client;
  return *this;
}

void nomad::Persistent_Property::set_content(const nlohmann::json &content) {
  property_content = content;
}

const nlohmann::json &nomad::Persistent_Property::get_content() const {
  return property_content;
}

bool nomad::Persistent_Property::send(const std::string &component) const {
  return iot_hub_client->send_property(
      serialize_read_only_property(property_name, property_content, component));
}

nomad::Property_Reporter::Property_Reporter(
    const std::string &name, const std::string &component, const int ver,
    const std::shared_ptr<IoT_Hub_Client_Adapter> &client)
    : property_name{name}, property_content{}, component{component},
      status{nomad::STATUS_INTERNAL_ERROR}, version{ver}, iot_hub_client{
                                                              client} {}

nomad::Property_Reporter::~Property_Reporter() { send_report(); }

void nomad::Property_Reporter::set_content(const nlohmann::json &content) {
  this->property_content = content;
}

void nomad::Property_Reporter::set_status(const Status status) {
  this->status = status;
}

void nomad::Property_Reporter::send_report() const {

  std::string description = (status == nomad::STATUS_SUCCESS)
                                ? g_success_description
                                : g_failure_description;
  iot_hub_client->send_property(
      serialize_writable_property(property_name, property_content, component,
                                  status, description, version));
}
