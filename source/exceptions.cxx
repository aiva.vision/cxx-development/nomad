#include "nomad/exceptions.hxx"

nomad::Exception::Exception(const std::string &m, Status s)
    : message(m), status{s} {}

const char *nomad::Exception::what() const noexcept { return message.c_str(); }

nomad::Status nomad::Exception::get_status() const noexcept { return status; }

nomad::Provisioning_Error::Provisioning_Error(const std::string &m)
    : Exception{std::string{"Provisioning failed with message: "} + m,
                STATUS_INTERNAL_ERROR} {}

nomad::Unset_Env_Var::Unset_Env_Var(const std::string &name)
    : Exception{std::string{"Environment variable "} + name + " is not set.",
                STATUS_EXTERNAL_ERROR} {}

nomad::Method_Processing_Error::Method_Processing_Error(
    const std::string &method, const std::string &m, Status s)
    : Exception{std::string{"Failed to process the method "} + method +
                    " with message: " + m,
                s} {}

nomad::Twin_Processing_Error::Twin_Processing_Error(const std::string &m,
                                                    Status s)
    : Exception{std::string{"Failed to process the received twin: "} + m, s} {}

nomad::Twin_Component_Processing_Error::Twin_Component_Processing_Error(
    const std::string &component, const std::string &message, Status s)
    : Exception(std::string("Failed to process twin component ") + component +
                    " with message: " + message,
                s) {}

nomad::Invalid_Location::Invalid_Location(const std::string &location,
                                          const std::string &message)
    : Exception{std::string{"Location "} + location +
                    " has been deemed invalid with message: " + message,
                STATUS_EXTERNAL_ERROR} {}

nomad::IoT_Hub_Client_Error::IoT_Hub_Client_Error(const std::string &m,
                                                  Status s)
    : Exception{std::string{"Error from the client adapter: "} + m, s} {}

nomad::Option_Set_Error::Option_Set_Error(const std::string &name,
                                          const int &result)
    : IoT_Hub_Client_Error{std::string{"Failed to set client option "} + name +
                               " with result " + std::to_string(result),
                           STATUS_INTERNAL_ERROR} {}

nomad::Unsupported_Connection_Type::Unsupported_Connection_Type(
    const std::string &connection_type)
    : IoT_Hub_Client_Error{std::string{"Connection type "} + connection_type +
                               " is not supported.",
                           STATUS_EXTERNAL_ERROR} {}

nomad::Property_Set_Error::Property_Set_Error(const std::string &name,
                                              const int &res)
    : Exception{std::string{"Failed to set message property "} + name +
                    " with result " + std::to_string(res),
                STATUS_INTERNAL_ERROR} {}

nomad::Message_Create_Error::Message_Create_Error()
    : Exception{"Failed to create message.", STATUS_INTERNAL_ERROR} {}
