#include "nomad/method_controller.hxx"
#include "nomad/exceptions.hxx"
#include "nomad/method_parser.hxx"
#include "nomad/method_response.hxx"

nomad::Method_Controller::Method_Controller(const std::shared_ptr<Model> &m)
    : model{m} {}

int nomad::Method_Controller::process_method(const std::string &method_name,
                                             const std::string &payload,
                                             Method_Response &response) {

  Method_Parser parser(method_name);
  auto parsed_payload = nlohmann::json::parse(payload);

  return model->process_method(parser.get_method(), parser.get_component(),
                               parsed_payload, response);
}
