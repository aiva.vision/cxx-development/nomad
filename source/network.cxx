#include "nomad/network.hxx"
#include <cstdlib>

nomad::Network::Status nomad::Network::status() {
  /*
     A return code different than zero indicates a failure.
     */
  if (std::system("wget -q --spider http://google.com")) {
    return Status::down;
  }
  return Status::up;
}
