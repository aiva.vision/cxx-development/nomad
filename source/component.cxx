#include <doctest/doctest.h>
#include <doctest/trompeloeil.hpp>
#include <fstream>

#include "nomad/component.hxx"
#include "nomad/exceptions.hxx"
#include "nomad/message.hxx"
#include "nomad/status.hxx"

const char *nomad::component_marker = "__t";

const char *nomad::default_component_name = "default";

nomad::CComponent::CComponent(
    const std::shared_ptr<IoT_Hub_Client_Adapter> &client,
    const std::string &name)
    : component_name{name}, iot_hub_client{client}, telemetries{},
      read_only_properties{} {}

void nomad::CComponent::acquire_telemetry(
    std::unique_ptr<Telemetry> &&telemetry) {
  telemetries.push_back(std::move(telemetry));
}

void nomad::CComponent::send_telemetry() const {
  for (auto &telemetry : telemetries) {
    telemetry->send(component_name);
  }
}

void nomad::CComponent::send_message(const std::string &name,
                                     const nlohmann::json &data) const {
  Message message(name, data);
  if (component_name != default_component_name) {
    message.set_component(name);
  }
  iot_hub_client->send_message(message);
}

void nomad::CComponent::acquire_read_only_property(
    std::unique_ptr<Read_Only_Property> &&property) {
  read_only_properties.push_back(std::move(property));
}

bool nomad::CComponent::send_read_only_properties() const {
  bool status = true;
  for (const auto &property : read_only_properties) {
    status &= property->send(component_name);
  }
  return status;
}

void nomad::CComponent::process_twin_component(const nlohmann::json &component,
                                               const int version) {

  if (component.contains(component_marker) &&
      component_name == default_component_name) {
    throw Twin_Component_Processing_Error(
        component_name,
        "The default component tried to parse a regular twin component.",
        STATUS_INTERNAL_ERROR);
  }

  if (!component.contains(component_marker) &&
      component_name != default_component_name) {
    throw Twin_Component_Processing_Error(
        component_name,
        "A regular component tried to parse a twin component "
        "intended for the default component.",
        STATUS_INTERNAL_ERROR);
  }

  for (auto &[key, value] : component.items()) {
    /*
       The coponent field is just a marker and should be ignored in this loop.
       */
    if (key == component_marker) {
      continue;
    }
    process_twin_property(key, value, version);
  }
}

class Mock_IoT_Hub_Client
    : public trompeloeil::mock_interface<nomad::IoT_Hub_Client_Adapter> {
  IMPLEMENT_MOCK1(set_method_controller);
  IMPLEMENT_MOCK1(set_twin_controller);
  IMPLEMENT_CONST_MOCK1(send_message);
  IMPLEMENT_CONST_MOCK1(send_property, noexcept);
  IMPLEMENT_MOCK0(do_work, noexcept);
};

SCENARIO("Processing a twin update for the default component.") {
  GIVEN("The default component.") {

    auto client = std::make_shared<Mock_IoT_Hub_Client>();
    ALLOW_CALL(*client, send_property(ANY(std::string &))).RETURN(true);
    ALLOW_CALL(*client, send_message(ANY(const nomad::Message &)));

    nomad::CComponent component(client);

    WHEN("The received twin component does not contain the component marker.") {

      nlohmann::json twin_component =
          nlohmann::json::parse(R"({"model_id":"peoplenet_v3_pruned.etlt"})");

      THEN("Processing the twin component should throw no exceptions.") {
        CHECK_NOTHROW(component.process_twin_component(twin_component, 1));
        CHECK_EQ(component.get_writable_property("model_id"),
                 "peoplenet_v3_pruned.etlt");
      }
    }

    WHEN("The received twin component does contain the component marker.") {

      nlohmann::json twin_component = nlohmann::json::parse(
          R"({"__t":"c","model_id":"peoplenet_v3_pruned.etlt"})");

      THEN("Processing the twin component should throw an exception.") {
        CHECK_THROWS_AS(component.process_twin_component(twin_component, 1),
                        nomad::Twin_Component_Processing_Error &);
      }
    }
  }
}

SCENARIO("Processing a twin update for a regular component.") {
  GIVEN("A regular component.") {

    auto client = std::make_shared<Mock_IoT_Hub_Client>();
    ALLOW_CALL(*client, send_property(ANY(std::string &))).RETURN(true);
    ALLOW_CALL(*client, send_message(ANY(const nomad::Message &)));

    nomad::CComponent component(client, "monitoring");

    WHEN("The received twin component does not contain the component marker.") {

      nlohmann::json twin_component =
          nlohmann::json::parse(R"({"model_id":"peoplenet_v3_pruned.etlt"})");

      THEN("Processing the twin component should throw an exception.") {
        CHECK_THROWS_AS(component.process_twin_component(twin_component, 1),
                        nomad::Twin_Component_Processing_Error &);
      }
    }

    WHEN("The received twin component does contain the component marker.") {

      nlohmann::json twin_component = nlohmann::json::parse(
          R"({"__t":"c","model_id":"peoplenet_v3_pruned.etlt"})");

      THEN("Processing the twin component should throw no exceptions.") {
        CHECK_NOTHROW(component.process_twin_component(twin_component, 1));
        CHECK_EQ(component.get_writable_property("model_id"),
                 "peoplenet_v3_pruned.etlt");
      }
    }
  }
}

void nomad::CComponent::process_twin_property(const std::string &name,
                                              const nlohmann::json &content,
                                              const int version) {
  Property_Reporter reporter(name, component_name, version, iot_hub_client);

  writable_properties.insert_or_assign(
      name, Writable_Property(name, content, iot_hub_client));

  reporter.set_status(nomad::STATUS_SUCCESS);
  reporter.set_content(content);
}

const nlohmann::json &
nomad::CComponent::get_writable_property(const std::string &name) const {
  return writable_properties.at(name).get_content();
}

void nomad::CComponent::set_persistent_property(const std::string &name,
                                                const nlohmann::json &content) {
  persistent_properties.insert_or_assign(
      name, Persistent_Property(name, content, iot_hub_client));
}

const nlohmann::json &
nomad::CComponent::get_persistent_property(const std::string &name) const {
  return persistent_properties.at(name).get_content();
}

SCENARIO("Setting and retrieving persistent properties.") {

  GIVEN("The default component.") {

    auto client = std::make_shared<Mock_IoT_Hub_Client>();

    nomad::CComponent component(client);

    WHEN("A persistent property is set.") {

      component.set_persistent_property("model_id", "peoplenet_v3_pruned.etlt");

      THEN("The same value should be returned when querying it.") {
        CHECK_EQ(component.get_persistent_property("model_id"),
                 "peoplenet_v3_pruned.etlt");
      }
    }
  }
}

bool nomad::CComponent::send_persistent_property(
    const std::string &name) const {
  return persistent_properties.at(name).send(component_name);
}

bool nomad::CComponent::send_persistent_properties() const {
  bool status = true;
  for (const auto &property : persistent_properties) {
    status &= property.second.send(component_name);
  }
  return status;
}

void nomad::CComponent::load_persistent_properties(
    const std::filesystem::path &location) {

  if (!std::filesystem::is_directory(location)) {
    throw Invalid_Location{location,
                           "The provided location is not a directory."};
  }
  auto file_name = location / (component_name + ".json");

  std::ifstream properties_file(file_name);

  if (!properties_file.is_open()) {
    throw Invalid_Location{
        location, std::string{"Failed to load the properties file from "} +
                      file_name.string() + "."};
  }

  nlohmann::json loaded_properties;
  properties_file >> loaded_properties;

  for (const auto &[key, value] : loaded_properties.items()) {
    if (!persistent_properties.contains(key)) {
      persistent_properties.insert_or_assign(
          key, Persistent_Property(key, value, iot_hub_client));
    }
  }

  properties_file.close();
}

void nomad::CComponent::save_persistent_properties(
    const std::filesystem::path &location) const {

  if (!std::filesystem::is_directory(location)) {
    throw Invalid_Location{location,
                           "The provided location is not a directory."};
  }
  auto file_name = location / (component_name + ".json");

  std::ofstream properties_file(file_name);
  if (!properties_file.is_open()) {
    throw Invalid_Location{location, std::string{"Failed to create "} +
                                         file_name.string() + "."};
  }
  nlohmann::json properties_to_save;

  for (const auto &property : persistent_properties) {
    properties_to_save[property.first] = property.second.get_content();
  }

  properties_file << std::setw(4) << properties_to_save << std::endl;
  properties_file.close();
}

void nomad::CComponent::acquire_method(std::unique_ptr<Method> &&method) {
  if (method->get_name().empty()) {
    throw Exception("Tried to acquire a method with an empty name.",
                    STATUS_EXTERNAL_ERROR);
  }
  methods.insert_or_assign(method->get_name(), std::move(method));
}

nomad::Status nomad::CComponent::execute_method(const std::string &method_name,
                                                const nlohmann::json &payload,
                                                Method_Response &response) {

  if (!has_method(method_name)) {
    throw Method_Processing_Error(method_name,
                                  std::string{"Method "} + method_name +
                                      " is not supported.",
                                  STATUS_EXTERNAL_ERROR);
  }
  return methods[method_name]->execute(payload, response, this);
}

bool nomad::CComponent::has_method(const std::string &method_name) const {
  return methods.contains(method_name);
}
