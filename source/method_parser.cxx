#include <cstring>
#include <doctest/doctest.h>
#include <doctest/trompeloeil.hpp>

#include "nomad/component.hxx"
#include "nomad/method_parser.hxx"

/*
   Character that separates a PnP component from the specific command on the
   component.
   */
static const char g_command_separator = '*';

nomad::Method_Parser::Method_Parser(const std::string &received_method) {

  auto separator = received_method.find(g_command_separator);

  if (separator == std::string::npos) {
    /*
       The separator character is optional.  If it is not present, it
       indicates a command of the root component and not a subcomponent (e.g.
       "reboot").
       */
    method = received_method;
    component = default_component_name;
  } else {
    /*
       If a separator character is present in the device method name, then a
       command on a subcomponent of the model is being targeted (e.g.
       management*update_model).
       */
    method = received_method.substr(separator + 1);
    component = received_method.substr(0, separator);
  }
}

SCENARIO("Parsing a received method with a separator.") {

  GIVEN("A method with a separator.") {

    const char *received_method = "management*update_model";

    WHEN("The parser is created.") {

      nomad::Method_Parser parser(received_method);

      THEN("We should get both a method name and a component name.") {

        CHECK_EQ(parser.get_method(), "update_model");
        CHECK_EQ(parser.get_component(), "management");
      }
    }
  }
}

SCENARIO("Parsing a received method without a separator.") {

  GIVEN("A method without a separator.") {

    const char *received_method = "update_model";

    WHEN("The parser is created.") {

      nomad::Method_Parser parser(received_method);

      THEN("We should get only a method name.") {

        CHECK_EQ(parser.get_method(), "update_model");
        CHECK_EQ(parser.get_component(), nomad::default_component_name);
      }
    }
  }
}

std::string &nomad::Method_Parser::get_method() { return method; }

std::string &nomad::Method_Parser::get_component() { return component; }
