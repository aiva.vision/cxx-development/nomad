#include <nlohmann/json.hpp>
#include <spdlog/spdlog.h>
#include <thread>

#include "azure_c_shared_utility/crt_abstractions.h"
#include "azure_prov_client/iothub_security_factory.h"
#include "azure_prov_client/prov_device_ll_client.h"
#include "azure_prov_client/prov_security_factory.h"
#include "azure_prov_client/prov_transport_mqtt_client.h"
#include "iothub_device_client_ll.h"
#include "iothubtransportmqtt.h"

#include "nomad/device_client_factory.hxx"
#include "nomad/exceptions.hxx"
#include "nomad/iot_hub_client_adapter.hxx"
#include "nomad/system.hxx"

using Provisioning_Instance_Info = struct PROV_INSTANCE_INFO_TAG;

/*
   Environment variable used to specify this application's DPS id scope
   */
static const char *g_dps_id_scope_environment_variable =
    "IOTHUB_DEVICE_DPS_ID_SCOPE";

/*
   Environment variable used to specify this application's DPS device id
   */
static const char *g_dps_device_id_environment_variable =
    "IOTHUB_DEVICE_DPS_DEVICE_ID";

/*
   Environment variable used to specify this application's DPS device key
   */
static const char *g_dps_device_key_environment_variable =
    "IOTHUB_DEVICE_DPS_DEVICE_KEY";

/*
   Environment variable used to optionally specify this application's DPS id
   scope
   */
static const char *g_dps_endpoint_environment_variable =
    "IOTHUB_DEVICE_DPS_ENDPOINT";
/*
   Enables tracing.
   */
static const char *g_enable_tracing_env_var = "IOTHUB_ENABLE_TRACING";

/*
   Global provisioning endpoint for DPS if one is not specified via the
   environment
   */
static const char g_dps_default_global_prov_uri[] =
    "global.azure-devices-provisioning.net";

/*
   Maximum amount of times we'll poll for DPS registration being ready.  Note
   that even though DPS works off of callbacks, the main() loop itself blocks
   */
static const int g_dps_registration_max_polls = 60;

/*
   Amount to sleep between querying state from DPS registration loop in
   milliseconds.
   */
static const std::chrono::milliseconds g_dps_registration_poll_sleep{1000};

const char *nomad::device_template_id_env_var = "IOTHUB_DEVICE_TEMPLATE_ID";

/*
   provisioning_register_callback is called back by the DPS client when the DPS
   server has either succeeded or failed our request.

   NOTE: remeber to free iothub_uri.
   */
static void
provisioning_register_callback(PROV_DEVICE_RESULT_TAG register_result,
                               const char *iothub_uri, const char *device_id,
                               void *user_context) {
  auto factory =
      static_cast<nomad::DPS_LL_Device_Client_Factory *>(user_context);

  if (register_result != PROV_DEVICE_RESULT_OK) {

    spdlog::error("DPS Provisioning callback called with error state {}.",
                  register_result);
    factory->set_registration_status(
        nomad::DPS_Registration_Status::Registration_Failed);

  } else {
    if (factory->set_iothub_uri(iothub_uri)) {

      spdlog::error("Unable to copy provisioning information.");
      factory->set_registration_status(
          nomad::DPS_Registration_Status::Registration_Failed);

    } else {

      spdlog::error("Provisioning callback indicates success.  "
                    "iothub_uri={}, device_id={}",
                    iothub_uri, device_id);

      factory->set_registration_status(
          nomad::DPS_Registration_Status::Registration_Succeeded);
    }
  }
}

bool nomad::is_tracing_enabled() {
  try {
    auto enable_tracing_env_var = System::env_var(g_enable_tracing_env_var);
    return strcmp(enable_tracing_env_var, "true") ? false : true;

  } catch (const Unset_Env_Var &e) {
    return false;
  }
}

nomad::DPS_LL_Device_Client_Factory::DPS_LL_Device_Client_Factory()
    : enable_tracing{is_tracing_enabled()}, template_id{System::env_var(
                                                device_template_id_env_var)},
      scope_id{System::env_var(g_dps_id_scope_environment_variable)},
      device_id{System::env_var(g_dps_device_id_environment_variable)},
      device_key{System::env_var(g_dps_device_key_environment_variable)},
      registration_status{DPS_Registration_Status::Registration_Not_Complete} {

  try {
    endpoint = System::env_var(g_dps_endpoint_environment_variable);

  } catch (const Unset_Env_Var &e) {
    endpoint = g_dps_default_global_prov_uri;
  }
}

nomad::DPS_LL_Device_Client_Factory::~DPS_LL_Device_Client_Factory() {

  if (iothub_uri) {
    free(iothub_uri);
  }
}

void nomad::DPS_LL_Device_Client_Factory::set_registration_status(
    DPS_Registration_Status status) {
  registration_status = status;
}

bool nomad::DPS_LL_Device_Client_Factory::set_iothub_uri(const char *uri) {
  return mallocAndStrcpy_s(&iothub_uri, uri) != 0;
}

void nomad::DPS_LL_Device_Client_Factory::register_device() {

  PROV_DEVICE_RESULT_TAG prov_device_result;
  nlohmann::json template_id_payload;
  template_id_payload["modelId"] = template_id;

  auto deleter = [](Provisioning_Instance_Info *info) {
    Prov_Device_LL_Destroy(info);
  };
  if ((prov_dev_set_symmetric_key_info(device_id, device_key) != 0)) {
    throw Provisioning_Error("prov_dev_set_symmetric_key_info failed");
  }
  if (prov_dev_security_init(SECURE_DEVICE_TYPE_SYMMETRIC_KEY) != 0) {
    throw Provisioning_Error("prov_dev_security_init failed");
  }

  std::unique_ptr<Provisioning_Instance_Info, decltype(deleter)>
      prov_instance_info{
          Prov_Device_LL_Create(endpoint, scope_id, Prov_Device_MQTT_Protocol),
          deleter};

  if (!prov_instance_info) {
    throw Provisioning_Error("Prov_Device_LL_Create failed");
  }
  if ((prov_device_result = Prov_Device_LL_SetOption(
           prov_instance_info.get(), PROV_OPTION_LOG_TRACE, &enable_tracing)) !=
      PROV_DEVICE_RESULT_OK) {
    throw Provisioning_Error("Setting provisioning tracing failed");
  }
  /*
     This step indicates the desired device template of the device to
     DPS.  This allows the service to (optionally) perform custom
     operations, such as allocating a different IoT Hub to devices
     based on their template id.
     */
  if ((prov_device_result = Prov_Device_LL_Set_Provisioning_Payload(
           prov_instance_info.get(), template_id_payload.dump().c_str())) !=
      PROV_DEVICE_RESULT_OK) {
    throw Provisioning_Error(
        "setting the template id as provisioning payload failed");
  }
  if ((prov_device_result = Prov_Device_LL_Register_Device(
           prov_instance_info.get(), provisioning_register_callback, this,
           nullptr, nullptr)) != PROV_DEVICE_RESULT_OK) {
    throw Provisioning_Error("Prov_Device_LL_Register_Device failed");
  }
  for (int i = 0; (i < g_dps_registration_max_polls) &&
                  (registration_status ==
                   DPS_Registration_Status::Registration_Not_Complete);
       i++) {

    Prov_Device_LL_DoWork(prov_instance_info.get());
    std::this_thread::sleep_for(g_dps_registration_poll_sleep);
  }
}

nomad::IoT_Hub_LL_Device_Client *
nomad::DPS_LL_Device_Client_Factory::create_ll_client() {

  register_device();

  if (registration_status == DPS_Registration_Status::Registration_Succeeded) {

    if (iothub_security_init(IOTHUB_SECURITY_TYPE_SYMMETRIC_KEY) != 0) {
      throw Provisioning_Error("iothub_security_init failed");

    } else {
      return IoTHubDeviceClient_LL_CreateFromDeviceAuth(iothub_uri, device_id,
                                                        MQTT_Protocol);
    }
  }
  return nullptr;
}

/*
   Environment variable used to specify this application's
   connection string.
   */
const char g_connection_string_environment_variable[] =
    "IOTHUB_DEVICE_CONNECTION_STRING";

nomad::DCS_LL_Device_Client_Factory::DCS_LL_Device_Client_Factory()
    : device_conn_string{
          System::env_var(g_connection_string_environment_variable)} {}

nomad::IoT_Hub_LL_Device_Client *
nomad::DCS_LL_Device_Client_Factory::create_ll_client() {

  return IoTHubDeviceClient_LL_CreateFromConnectionString(device_conn_string,
                                                          MQTT_Protocol);
}
