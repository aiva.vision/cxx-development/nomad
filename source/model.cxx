#include <doctest/doctest.h>
#include <doctest/trompeloeil.hpp>

#include "nomad/exceptions.hxx"
#include "nomad/model.hxx"

/*
   The version of the desired twin is represented by the $version metadata.
   */
static const char g_iothub_twin_desired_version[] = "$version";

nomad::CModel::CModel(const std::shared_ptr<IoT_Hub_Client_Adapter> &client)
    : iot_hub_client{client} {}

bool nomad::CModel::has_component(const std::string &name) const {
  return components.contains(name);
}

void nomad::CModel::send_read_only_properties() const {
  for (auto &component : components) {
    component.second->send_read_only_properties();
  }
}

void nomad::CModel::load_persistent_properties(
    const std::filesystem::path &location) {
  for (const auto &comp : components) {
    comp.second->load_persistent_properties(location);
  }
}

void nomad::CModel::save_persistent_properties(
    const std::filesystem::path &location) const {
  for (const auto &comp : components) {
    comp.second->save_persistent_properties(location);
  }
}

void nomad::CModel::send_persistent_properties() const {
  for (const auto &comp : components) {
    comp.second->send_persistent_properties();
  }
}

const nlohmann::json &
nomad::CModel::get_persistent_property(const std::string &name,
                                       const std::string &component) const {
  return components.at(component)->get_persistent_property(name);
}

void nomad::CModel::set_persistent_property(const std::string &name,
                                            const nlohmann::json &content,
                                            const std::string &component) {

  components.at(component)->set_persistent_property(name, content);
}

void nomad::CModel::send_telemetry() const {

  for (auto &component : components) {
    component.second->send_telemetry();
  }
}

void nomad::CModel::send_message(const std::string &name,
                                 const nlohmann::json &content,
                                 const std::string &component) const {
  components.at(component)->send_message(name, content);
}

void nomad::CModel::acquire_component(std::unique_ptr<Component> &&component) {
  components.insert_or_assign(component->get_name(), std::move(component));
}

nomad::Status nomad::CModel::process_method(const std::string &method_name,
                                            const std::string &component_name,
                                            const nlohmann::json &payload,
                                            Method_Response &response) {

  if (!has_component(component_name)) {
    throw Method_Processing_Error(method_name,
                                  std::string{"Component "} + component_name +
                                      " is not part of the model.",
                                  STATUS_NOT_FOUND);
  }
  return components[component_name]->execute_method(method_name, payload,
                                                    response);
}

static bool is_component(const nlohmann::json &member) noexcept {
  if (!member.is_object()) {
    return false;
  }
  if (!member.contains(nomad::component_marker)) {
    return false;
  }
  return true;
}

void nomad::CModel::process_twin_data(const nlohmann::json &data) {

  if (!(data.contains(g_iothub_twin_desired_version))) {
    throw Twin_Processing_Error(std::string{"Can not retrieve "} +
                                    g_iothub_twin_desired_version +
                                    " field for twin",
                                STATUS_BAD_FORMAT);
  } else if (!data[g_iothub_twin_desired_version].is_number()) {
    throw Twin_Processing_Error(std::string{"JSON field "} +
                                    g_iothub_twin_desired_version +
                                    " is not a number",
                                STATUS_BAD_FORMAT);
  } else {
    int version = data[g_iothub_twin_desired_version];

    for (const auto &member : data.items()) {
      if (member.key() == g_iothub_twin_desired_version) {
        /*
           The version field is metadata and should be ignored in this loop.
           */
        continue;
      } else if (is_component(member.value())) {
        /*
           If a valid component is received, it must be present in the model.
           */
        if (!has_component(member.key())) {
          throw Twin_Processing_Error(std::string{"Component "} + member.key() +
                                          " was not found in the model.",
                                      STATUS_NOT_FOUND);
        }
        /*
           If this current JSON is an element AND the name is one of the
           model's components that the application knows about, then this json
           element represents a component.
           */
        components[member.key()]->process_twin_component(member.value(),
                                                         version);
      } else {
        /*
           If the child element is not a component, this is a property of the
           model's root component. Therefore, we have to wrapp it as a json and
           send it to the default component.
           */
        components[default_component_name]->process_twin_property(
            member.key(), member.value(), version);
      }
    }
  }
}

class Mock_IoT_Hub_Client
    : public trompeloeil::mock_interface<nomad::IoT_Hub_Client_Adapter> {
  IMPLEMENT_MOCK1(set_method_controller);
  IMPLEMENT_MOCK1(set_twin_controller);
  IMPLEMENT_CONST_MOCK1(send_message, noexcept);
  IMPLEMENT_CONST_MOCK1(send_property, noexcept);
  IMPLEMENT_MOCK0(do_work, noexcept);
};

class Mock_Component : public trompeloeil::mock_interface<nomad::Component> {
public:
  IMPLEMENT_CONST_MOCK0(get_name);

  IMPLEMENT_MOCK1(acquire_telemetry);
  IMPLEMENT_CONST_MOCK0(send_telemetry);
  IMPLEMENT_CONST_MOCK2(send_message);

  IMPLEMENT_CONST_MOCK0(send_read_only_properties);

  IMPLEMENT_MOCK1(acquire_read_only_property);
  IMPLEMENT_MOCK2(process_twin_component);
  IMPLEMENT_MOCK3(process_twin_property);
  IMPLEMENT_CONST_MOCK1(get_writable_property);

  IMPLEMENT_MOCK2(set_persistent_property);
  IMPLEMENT_CONST_MOCK1(get_persistent_property);
  IMPLEMENT_CONST_MOCK1(send_persistent_property);
  IMPLEMENT_CONST_MOCK0(send_persistent_properties);
  IMPLEMENT_MOCK1(load_persistent_properties);
  IMPLEMENT_CONST_MOCK1(save_persistent_properties);

  IMPLEMENT_MOCK1(acquire_method);
  IMPLEMENT_MOCK3(execute_method);
  IMPLEMENT_CONST_MOCK1(has_method);
};

SCENARIO("Processing the desired twin.") {

  GIVEN("A device model.") {

    auto client = std::make_shared<Mock_IoT_Hub_Client>();

    auto default_component = std::make_unique<Mock_Component>();
    ALLOW_CALL(*default_component, get_name())
        .RETURN(nomad::default_component_name);
    ALLOW_CALL(*default_component,
               process_twin_property(ANY(const std::string &),
                                     ANY(const nlohmann::json &),
                                     ANY(const int)));
    ALLOW_CALL(
        *default_component,
        process_twin_component(ANY(const nlohmann::json &), ANY(const int)));

    auto monitoring_component = std::make_unique<Mock_Component>();
    ALLOW_CALL(*monitoring_component, get_name()).RETURN("monitoring");
    ALLOW_CALL(*monitoring_component,
               process_twin_property(ANY(const std::string &),
                                     ANY(const nlohmann::json &),
                                     ANY(const int)));
    ALLOW_CALL(
        *monitoring_component,
        process_twin_component(ANY(const nlohmann::json &), ANY(const int)));

    nomad::CModel model{client};
    model.acquire_component(std::move(default_component));
    model.acquire_component(std::move(monitoring_component));

    WHEN("The received twin does not contain the component marker (refers to "
         "the default component).") {

      nlohmann::json twin =
          nlohmann::json::parse(R"({"max_temperature":90.7,"$version":3})");

      THEN("No exceptions should be thrown.") {

        CHECK_NOTHROW(model.process_twin_data(twin));
      }
      AND_WHEN("The twin does not contain the version field.") {

        nlohmann::json twin =
            nlohmann::json::parse(R"({"max_temperature":90.7})");

        THEN("A twin processing exception should be thrown.") {

          CHECK_THROWS_AS(model.process_twin_data(twin),
                          nomad::Twin_Processing_Error &);
        }
      }
    }
    WHEN("The received twin does contain the component marker and it is part "
         "of the model.") {

      nlohmann::json twin = nlohmann::json::parse(
          R"({"monitoring":{"max_temperature":90.7,"__t":"c"},"$version":3})");

      THEN("No exceptions should be thrown.") {

        CHECK_NOTHROW(model.process_twin_data(twin));
      }
      AND_WHEN("The twin does not contain the version field.") {

        nlohmann::json twin = nlohmann::json::parse(
            R"({"monitoring":{"max_temperature":90.7,"__t":"c"}})");

        THEN("A twin processing exception should be thrown.") {

          CHECK_THROWS_AS(model.process_twin_data(twin),
                          nomad::Twin_Processing_Error &);
        }
      }
    }
    WHEN("The received twin does contain the component marker and it is not "
         "part of the model.") {

      nlohmann::json twin = nlohmann::json::parse(
          R"({"unknown":{"overcrowding_treshold":100,"__t":"c"},"$version":3})");

      THEN("A twin processing exception should be thrown.") {

        CHECK_THROWS_AS(model.process_twin_data(twin),
                        nomad::Twin_Processing_Error &);
      }
      AND_WHEN("The twin does not contain the version field.") {

        nlohmann::json twin = nlohmann::json::parse(
            R"({"unknown":{"overcrowding_treshold":100,"__t":"c"},"$version":3})");

        THEN("A twin processing exception should be thrown.") {

          CHECK_THROWS_AS(model.process_twin_data(twin),
                          nomad::Twin_Processing_Error &);
        }
      }
    }
  }
}

void nomad::CModel::do_work() noexcept { iot_hub_client->do_work(); }
