#include "azure/storage/blobs.hpp"

#include <filesystem>
#include <regex>

#include "nomad/blob_storage.hxx"
#include "nomad/exceptions.hxx"
#include "nomad/file.hxx"

static const std::string &get_blob_storage_connection_string() {

  const static std::string env_connection_string =
      std::getenv("AZURE_STORAGE_CONNECTION_STRING");

  if (!env_connection_string.empty()) {
    return env_connection_string;
  }
  throw nomad::Exception("Can not find connection string.",
                         nomad::STATUS_EXTERNAL_ERROR);
}

void nomad::upload_file(const std::filesystem::path &file,
                        const std::string &container_name,
                        const bool delete_after, const bool not_open) {

  if (not_open && File::status(file) != File::Status::not_open) {
    throw nomad::Exception{"Failed to upload the file '" + file.string() +
                               "' because it is opened by another process",
                           STATUS_EXTERNAL_ERROR};
  }
  auto container_client =
      Azure::Storage::Blobs::BlobContainerClient::CreateFromConnectionString(
          get_blob_storage_connection_string(), container_name);

  container_client.CreateIfNotExists();

  auto blob_client = container_client.GetBlockBlobClient(file.filename());

  try {
    blob_client.UploadFrom(file);

  } catch (const std::runtime_error &e) {

    throw nomad::Exception{"Failed to upload the file '" + file.string() +
                               "' with exception: " + e.what(),
                           STATUS_EXTERNAL_ERROR};
  }
  if (delete_after) {
    std::filesystem::remove(file);
  }
}

unsigned nomad::upload_files(const std::filesystem::path &source,
                             const std::string &container_name,
                             const bool empty_after, const bool not_open) {

  auto container_client =
      Azure::Storage::Blobs::BlobContainerClient::CreateFromConnectionString(
          get_blob_storage_connection_string(), container_name);

  container_client.CreateIfNotExists();
  unsigned upload_count = 0;

  for (auto &file : std::filesystem::directory_iterator(source)) {

    if (not_open && File::status(file) != File::Status::not_open) {
      continue;
    }
    auto blob_client =
        container_client.GetBlockBlobClient(file.path().filename());

    try {
      blob_client.UploadFrom(file.path());

    } catch (const std::runtime_error &e) {

      throw nomad::Exception{std::string{"Failed to upload the file '"} +
                                 file.path().string() +
                                 "' with exception: " + e.what(),
                             STATUS_EXTERNAL_ERROR};
    }
    if (empty_after) {
      std::filesystem::remove(file);
    }
    ++upload_count;
  }
  return upload_count;
}

void nomad::download_file(const std::filesystem::path &destination,
                          const std::string &blob_name,
                          const std::string &container_name) {

  auto container_client =
      Azure::Storage::Blobs::BlobContainerClient::CreateFromConnectionString(
          get_blob_storage_connection_string(), container_name);

  try {

    auto blob_client = container_client.GetBlockBlobClient(blob_name);
    blob_client.DownloadTo(destination / blob_name);

  } catch (const Azure::Core::RequestFailedException &e) {

    throw Exception(std::string{"Failed to download blob "} + blob_name +
                        " with error: " + e.what(),
                    STATUS_EXTERNAL_ERROR);
  }
}

unsigned nomad::download_files(const std::filesystem::path &destination,
                               const std::vector<std::string> &blobs,
                               const std::string &container_name) {

  auto container_client =
      Azure::Storage::Blobs::BlobContainerClient::CreateFromConnectionString(
          get_blob_storage_connection_string(), container_name);

  for (const std::string &blob_name : blobs) {

    try {

      auto blob_client = container_client.GetBlockBlobClient(blob_name);
      blob_client.DownloadTo(destination / blob_name);

    } catch (const Azure::Core::RequestFailedException &e) {

      throw Exception(std::string{"Failed to download blob "} + blob_name +
                          " with error: " + e.what(),
                      STATUS_EXTERNAL_ERROR);
    }
  }
  return blobs.size();
}
