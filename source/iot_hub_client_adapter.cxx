#include <cstdlib>
#include <cstring>
#include <mutex>
#include <nlohmann/json.hpp>
#include <spdlog/spdlog.h>
#include <string>

#include "iothub.h"
#include "iothub_client_core_common.h"
#include "iothub_client_options.h"
#include "iothub_device_client_ll.h"
#include "iothub_module_client_ll.h"
#include "iothubtransportmqtt.h"

#include "nomad/exceptions.hxx"
#include "nomad/iot_hub_client_adapter.hxx"
#include "nomad/method_controller.hxx"
#include "nomad/system.hxx"
#include "nomad/twin_controller.hxx"

const char *warning_telemetry_name = "warning";
const char *error_telemetry_name = "error";

/*
   Connection type used in the simple factory method.
   */
static const char *g_connection_type_env_var_name = "IOTHUB_CONNECTION_TYPE";

static bool g_url_auto_encode_decode = true;

static std::once_flag iot_hub_flag;

/*
   Before invoking ANY IoT Hub or DPS functionality, IoTHub_Init must be
   invoked.
   */
static void initialize_iot_hub() {
  std::call_once(iot_hub_flag, []() {
    if (IoTHub_Init()) {
      throw nomad::Exception{"Failed to initialize IoT Hub",
                             nomad::STATUS_INTERNAL_ERROR};
    }
  });
}

static void deinitialize_iot_hub() {
  std::call_once(iot_hub_flag, IoTHub_Deinit);
}

nomad::Device_Client_Adapter::Device_Client_Adapter(
    Device_Client_Adapter &&client) {

  if (ll_device_client) {
    IoTHubDeviceClient_LL_Destroy(ll_device_client);
  }
  ll_device_client = client.ll_device_client;
  client.ll_device_client = nullptr;
}

/*
   Invoked by IoT SDK when a device method arrives. It is registered by the IoT
   Hub client as a callback with a Method_Controller object as user context.
   This is necessary because of the C99 implementation of the azure-iot-c-sdk.
   */
static int nomad_method_callback(const char *method_name,
                                 const unsigned char *payload, size_t size,
                                 unsigned char **response,
                                 size_t *response_size,
                                 void *user_context_callback) noexcept {

  nomad::Method_Response method_response(response, response_size);

  auto controller =
      static_cast<nomad::Method_Controller *>(user_context_callback);

  try {
    return controller->process_method(
        method_name, std::string(reinterpret_cast<const char *>(payload), size),
        method_response);

  } catch (const nomad::Method_Processing_Error &e) {

    spdlog::error("An exception occured: {}", e.what());
    return e.get_status();

  } catch (const std::exception &e) {
    spdlog::error("Unexpected exception occured: {}", e.what());

  } catch (...) {
    spdlog::error("An unknown exception occured.");
  }
  return nomad::STATUS_INTERNAL_ERROR;
}

/*
   Invoked by IoT SDK when a twin - either full twin or a PATCH update arrives.
   */
static void nomad_twin_callback(DEVICE_TWIN_UPDATE_STATE update_state,
                                const unsigned char *payload, size_t size,
                                void *user_context_callback) noexcept {
  auto controller =
      static_cast<nomad::Twin_Controller *>(user_context_callback);

  nomad::Twin_Update_State twin_state;

  switch (update_state) {
  case DEVICE_TWIN_UPDATE_COMPLETE:
    twin_state = nomad::Twin_Update_State::complete;
    break;
  case DEVICE_TWIN_UPDATE_PARTIAL:
    twin_state = nomad::Twin_Update_State::partial;
    break;
  default:
    spdlog::error("Unknown twin update state received.");
    return;
  }

  try {
    controller->process_received_twin(
        twin_state, std::string(reinterpret_cast<const char *>(payload), size));

  } catch (const nomad::Twin_Processing_Error &e) {
    spdlog::error("An exception occured: {}", e.what());

  } catch (const std::exception &e) {
    spdlog::error("An unexpected exception occured: {}", e.what());

  } catch (...) {
    spdlog::error("An unexpected exception occured.");
  }
}

nomad::Device_Client_Adapter::Device_Client_Adapter(
    LL_Device_Client_Factory &client_factory) {

  initialize_iot_hub();

  auto iothub_result = IOTHUB_CLIENT_OK;

  if (!(ll_device_client = client_factory.create_ll_client())) {
    throw IoT_Hub_Client_Error{"Failed to create low level device client",
                               STATUS_INTERNAL_ERROR};
  }
  /*
     Sets verbosity level
     */
  else if (bool enable_tracing = is_tracing_enabled();
           (iothub_result = IoTHubDeviceClient_LL_SetOption(
                ll_device_client, OPTION_LOG_TRACE, &enable_tracing)) !=
           IOTHUB_CLIENT_OK) {
    throw Option_Set_Error{OPTION_LOG_TRACE, iothub_result};
  }
  /*
     Sets the name of ModelId for this PnP device.
     This *MUST* be set before the client is connected to IoTHub.  We do not
     automatically connect when the handle is created, but will implicitly
     connect to subscribe for device method and device twin callbacks below.
     */
  else if (const char *template_id =
               System::env_var(device_template_id_env_var);
           template_id && (iothub_result = IoTHubDeviceClient_LL_SetOption(
                               ll_device_client, OPTION_MODEL_ID,
                               template_id)) != IOTHUB_CLIENT_OK) {
    throw Option_Set_Error{OPTION_MODEL_ID, iothub_result};
  }
  /*
     Enabling auto url encode will have the underlying SDK perform URL encoding
     operations automatically.
     */
  else if ((iothub_result = IoTHubDeviceClient_LL_SetOption(
                ll_device_client, OPTION_AUTO_URL_ENCODE_DECODE,
                &g_url_auto_encode_decode)) != IOTHUB_CLIENT_OK) {
    throw Option_Set_Error{OPTION_AUTO_URL_ENCODE_DECODE, iothub_result};
  }
}

nomad::Device_Client_Adapter::~Device_Client_Adapter() {
  IoTHubDeviceClient_LL_Destroy(ll_device_client);
  deinitialize_iot_hub();
}

void nomad::Device_Client_Adapter::set_method_controller(
    Method_Controller *controller) {

  auto iothub_result = IoTHubDeviceClient_LL_SetDeviceMethodCallback(
      ll_device_client, nomad_method_callback, controller);

  if (iothub_result != IOTHUB_CLIENT_OK) {

    throw IoT_Hub_Client_Error{
        "Failed to set the method controller for the device client",
        STATUS_INTERNAL_ERROR};
  }
}

void nomad::Device_Client_Adapter::set_twin_controller(
    Twin_Controller *controller) {

  auto iothub_result = IoTHubDeviceClient_LL_SetDeviceTwinCallback(
      ll_device_client, nomad_twin_callback, controller);

  if (iothub_result != IOTHUB_CLIENT_OK) {

    throw IoT_Hub_Client_Error{
        "Failed to set the twin controller for the device client",
        STATUS_INTERNAL_ERROR};
  }
}

void nomad::Device_Client_Adapter::send_message(const Message &message) const {

  auto iothub_result = IoTHubDeviceClient_LL_SendEventAsync(
      ll_device_client, message.get_handle(), NULL, NULL);

  if (iothub_result != IOTHUB_CLIENT_OK) {
    throw Exception("Failed to send message from the device client adapter",
                    STATUS_INTERNAL_ERROR);
  }
}

bool nomad::Device_Client_Adapter::send_property(
    const std::string &serialized_property) const noexcept {

  auto client_result = IoTHubDeviceClient_LL_SendReportedState(
      ll_device_client,
      reinterpret_cast<const unsigned char *>(serialized_property.c_str()),
      serialized_property.size(), NULL, NULL);

  if (client_result != IOTHUB_CLIENT_OK) {
    return false;
  }
  return true;
}

void nomad::Device_Client_Adapter::do_work() noexcept {
  IoTHubDeviceClient_LL_DoWork(ll_device_client);
}

nomad::Module_Client_Adapter::Module_Client_Adapter(
    Module_Client_Adapter &&client) {
  if (ll_module_client) {
    IoTHubModuleClient_LL_Destroy(ll_module_client);
  }
  ll_module_client = client.ll_module_client;
  client.ll_module_client = nullptr;
}

nomad::Module_Client_Adapter::Module_Client_Adapter() {
  initialize_iot_hub();
  if (!(ll_module_client =
            IoTHubModuleClient_LL_CreateFromEnvironment(MQTT_Protocol))) {
    throw IoT_Hub_Client_Error{
        "Failed to create low level module client from the environment",
        STATUS_INTERNAL_ERROR};
  }

  auto iothub_result = IOTHUB_CLIENT_OK;

  /*
     Sets verbosity level
     */
  if (bool enable_tracing = is_tracing_enabled();
      (iothub_result = IoTHubModuleClient_LL_SetOption(
           ll_module_client, OPTION_LOG_TRACE, &enable_tracing)) !=
      IOTHUB_CLIENT_OK) {

    throw Option_Set_Error{OPTION_LOG_TRACE, iothub_result};
  }
  /*
     Enabling auto url encode will have the underlying SDK perform URL
     encoding operations automatically.
     */
  else if ((iothub_result = IoTHubModuleClient_LL_SetOption(
                ll_module_client, OPTION_AUTO_URL_ENCODE_DECODE,
                &g_url_auto_encode_decode)) != IOTHUB_CLIENT_OK) {

    throw Option_Set_Error{OPTION_AUTO_URL_ENCODE_DECODE, iothub_result};
  }
}

nomad::Module_Client_Adapter::~Module_Client_Adapter() {

  IoTHubModuleClient_LL_Destroy(ll_module_client);
  IoTHub_Deinit();
}

void nomad::Module_Client_Adapter::set_method_controller(
    Method_Controller *controller) {

  auto iothub_result = IoTHubModuleClient_LL_SetModuleMethodCallback(
      ll_module_client, nomad_method_callback, controller);

  if (iothub_result != IOTHUB_CLIENT_OK) {

    throw IoT_Hub_Client_Error{
        "Failed to set the method controller for the module client",
        STATUS_INTERNAL_ERROR};
  }
}

void nomad::Module_Client_Adapter::set_twin_controller(
    Twin_Controller *controller) {

  auto iothub_result = IoTHubModuleClient_LL_SetModuleTwinCallback(
      ll_module_client, nomad_twin_callback, controller);

  if (iothub_result != IOTHUB_CLIENT_OK) {

    throw IoT_Hub_Client_Error{
        "Failed to set the twin controller for the module client",
        STATUS_INTERNAL_ERROR};
  }
}

void nomad::Module_Client_Adapter::send_message(const Message &message) const {

  auto iothub_result = IoTHubModuleClient_LL_SendEventAsync(
      ll_module_client, message.get_handle(), NULL, NULL);

  if (iothub_result != IOTHUB_CLIENT_OK) {
    throw Exception("Failed to send message from the module client adapter",
                    STATUS_INTERNAL_ERROR);
  }
}

bool nomad::Module_Client_Adapter::send_property(
    const std::string &serialized_property) const noexcept {

  auto client_result = IoTHubModuleClient_LL_SendReportedState(
      ll_module_client,
      reinterpret_cast<const unsigned char *>(serialized_property.c_str()),
      serialized_property.size(), NULL, NULL);

  if (client_result != IOTHUB_CLIENT_OK) {
    return false;
  }
  return true;
}

void nomad::Module_Client_Adapter::do_work() noexcept {
  IoTHubModuleClient_LL_DoWork(ll_module_client);
}

std::shared_ptr<nomad::IoT_Hub_Client_Adapter> nomad::create_iot_hub_client() {

  const char *iot_hub_connection_type =
      System::env_var(g_connection_type_env_var_name);

  if (!strcmp(iot_hub_connection_type, "EDGE")) {

    return std::make_shared<nomad::Module_Client_Adapter>();

  } else if (!strcmp(iot_hub_connection_type, "DPS")) {

    nomad::DPS_LL_Device_Client_Factory factory{};
    return std::make_shared<nomad::Device_Client_Adapter>(factory);

  } else if (strcmp(iot_hub_connection_type, "DCS")) {

    nomad::DCS_LL_Device_Client_Factory factory{};
    return std::make_shared<nomad::Device_Client_Adapter>(factory);

  } else {
    throw nomad::Unsupported_Connection_Type{iot_hub_connection_type};
  }
}
