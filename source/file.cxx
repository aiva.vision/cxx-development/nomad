#include "nomad/file.hxx"
#include "nomad/system.hxx"

nomad::File::Status nomad::File::status(const std::filesystem::path &file) {

  auto awk_output = System::exec_shell(
      (std::string{"lsof -f -- "} + file.string() + " 2>/dev/null | awk '" +
       "/" + file.stem().string() + "/ {print $4}'"));

  if (awk_output.find('r') != std::string::npos) {
    if (awk_output.find('w') != std::string::npos) {
      return Status::read_write;
    }
    return Status::read;
  }
  if (awk_output.find('w') != std::string::npos) {
    if (awk_output.find('r') != std::string::npos) {
      return Status::read_write;
    }
    return Status::write;
  }
  if (awk_output.find('u') != std::string::npos) {
    return Status::read_write;
  }
  if (awk_output.find(' ') != std::string::npos) {
    return Status::unknown;
  }
  if (awk_output.find('-') != std::string::npos) {
    return Status::unknown_and_lock_char;
  }
  return Status::not_open;
}
