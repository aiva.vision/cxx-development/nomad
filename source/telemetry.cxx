#include <utility>

#include "nomad/component.hxx"
#include "nomad/message.hxx"
#include "nomad/telemetry.hxx"

nomad::Basic_Telemetry::Basic_Telemetry(
    const std::shared_ptr<IoT_Hub_Client_Adapter> &client)
    : iot_hub_client{client} {}

void nomad::Basic_Telemetry::send(const std::string &component) const {

  Message message{get_telemetry_data()};

  if (component != default_component_name) {
    message.set_component(component);
  }
  iot_hub_client->send_message(message);
}

nomad::Compound_Telemetry::Compound_Telemetry(
    const std::shared_ptr<IoT_Hub_Client_Adapter> &client)
    : iot_hub_client{client} {}

void nomad::Compound_Telemetry::send(const std::string &component) const {

  for (const auto &telemetry_data : get_telemetry_data()) {

    Message message{telemetry_data};

    if (component != default_component_name) {
      message.set_component(component);
    }
    iot_hub_client->send_message(message);
  }
}
