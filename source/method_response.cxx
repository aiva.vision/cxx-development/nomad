#include <cstdlib>
#include <cstring>
#include <ostream>
#include <spdlog/spdlog.h>
#include <stdexcept>

#include "nomad/method_response.hxx"

/*
   An empty JSON body for PnP command responses
   */
static const char g_empty_json[] = "{}";

nomad::Method_Response::Method_Response(unsigned char **response,
                                        size_t *response_size) {
  this->response = response;
  *this->response = nullptr;
  this->response_size = response_size;
  *this->response_size = 0;
}

nomad::Method_Response::~Method_Response() {
  if (*response == nullptr) {
    try {
      set(g_empty_json);
    } catch (const std::exception &e) {
      spdlog::error(
          "An exception occured when setting the default method response: {}",
          e.what());
    }
  }
}

void nomad::Method_Response::set(const nlohmann::json &content) {

  if (*response) {

    free(*response);
    *response_size = 0;
  }
  auto data = content.dump();

  if (!(*response = static_cast<unsigned char *>(calloc(1, data.size())))) {

    throw std::runtime_error{
        "Failed to allocate memory for new method response."};

  } else {

    memcpy(*response, data.c_str(), data.size());
    *response_size = data.size();
  }
}
