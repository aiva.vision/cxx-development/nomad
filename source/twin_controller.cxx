#include <doctest/doctest.h>
#include <doctest/trompeloeil.hpp>
#include <nlohmann/json.hpp>

#include "nomad/exceptions.hxx"
#include "nomad/twin_controller.hxx"

/*
   Name of desired JSON field when retrieving a full twin.
   */
static const char *g_iothub_twin_desired_object_name = "desired";

nomad::Twin_Controller::Twin_Controller(std::shared_ptr<Model> model) {
  this->model = model;
}

void nomad::Twin_Controller::process_received_twin(Twin_Update_State state,
                                                   const std::string &payload) {

  nlohmann::json desired_object = get_desired_object(state, payload);

  if (desired_object.empty()) {
    throw Twin_Processing_Error("The desired object extraction failed.",
                                STATUS_BAD_FORMAT);
  }

  model->process_twin_data(desired_object);
}

class Mock_Model : public trompeloeil::mock_interface<nomad::Model> {
public:
  IMPLEMENT_MOCK1(acquire_component);
  IMPLEMENT_CONST_MOCK1(has_component);

  IMPLEMENT_CONST_MOCK0(send_telemetry);
  IMPLEMENT_CONST_MOCK3(send_message);
  IMPLEMENT_CONST_MOCK0(send_read_only_properties);

  IMPLEMENT_MOCK1(load_persistent_properties);
  IMPLEMENT_CONST_MOCK1(save_persistent_properties);
  IMPLEMENT_CONST_MOCK0(send_persistent_properties);
  IMPLEMENT_CONST_MOCK2(get_persistent_property);
  IMPLEMENT_MOCK3(set_persistent_property);

  IMPLEMENT_MOCK4(process_method);
  IMPLEMENT_MOCK1(process_twin_data);
  IMPLEMENT_MOCK0(do_work, noexcept);
};

SCENARIO("Processing the received twin.") {

  GIVEN("A twin controller.") {

    auto model = std::make_shared<Mock_Model>();
    ALLOW_CALL(*model, process_twin_data(ANY(const nlohmann::json &)));

    nomad::Twin_Controller controller(model);

    WHEN("The twin controller recieves a twin.") {

      std::string payload =
          R"({"monitoring":{"max_temperature":90.7,"__t":"c"},"$version":3})";

      THEN("No exceptions should be thrown.") {

        CHECK_NOTHROW(controller.process_received_twin(
            nomad::Twin_Update_State::partial, payload));
      }
    }
  }
}

nlohmann::json
nomad::Twin_Controller::get_desired_object(Twin_Update_State state,
                                           const std::string &payload) {

  nlohmann::json root_object = nlohmann::json::parse(payload);

  switch (state) {
  case Twin_Update_State::complete:
    /*
       For a complete update, the JSON from IoTHub will contain both "desired"
       and "reported" - the full twin. We only care about "desired" in this
       sample, so just retrieve it.
       */
    return root_object[g_iothub_twin_desired_object_name];
    break;
  case Twin_Update_State::partial:
    /*
       For a patch update, IoTHub does not explicitly put a "desired:" JSON
       envelope.  The "desired-ness" is implicit in this case, so here we
       simply need the root of the JSON itself.
       */
    return root_object;
    break;
  }
  throw std::runtime_error{
      "Invalid twin update state received by the twin controller."};
}
