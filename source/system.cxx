#include <cstring>
#include <string>
#include <unistd.h>

#include "nomad/exceptions.hxx"
#include "nomad/system.hxx"

const char *nomad::System::env_var(const char *name) {

  auto value = getenv(name);

  if (!value) {
    throw nomad::Unset_Env_Var(name);
  }
  return value;
}

std::string nomad::System::timestamp() {

  time_t tloc;
  struct tm tm_log;
  struct timespec ts;
  char strmsec[6]; //.nnnZ\0

  const unsigned max_timestamp_length = 64;

  auto timestamp = std::make_unique<char[]>(max_timestamp_length + 1);

  clock_gettime(CLOCK_REALTIME, &ts);
  memcpy(&tloc, (void *)(&ts.tv_sec), sizeof(time_t));
  gmtime_r(&tloc, &tm_log);
  strftime(timestamp.get(), max_timestamp_length, "%Y-%m-%dT%H:%M:%S", &tm_log);
  int ms = ts.tv_nsec / 1000000;
  snprintf(strmsec, sizeof(strmsec), ".%.3dZ", ms);
  strncat(timestamp.get(), strmsec, max_timestamp_length);

  return std::string{timestamp.get()};
}

class Process_Stream {
public:
  Process_Stream(const std::string &cmd) {
    process_stream = popen(cmd.c_str(), "r");
  }

  ~Process_Stream() { pclose(process_stream); }

  FILE *get() { return process_stream; }

private:
  FILE *process_stream;
};

std::string nomad::System::exec_shell(const std::string &command) {

  /* Redirect the standard error to the standard output. */
  Process_Stream pstream(command + " 2>&1");

  if (!pstream.get())
    return std::string{};

  size_t chread;
  char buffer[256];
  std::string shell_output;

  /* Use fread so binary data is dealt with correctly */
  while ((chread = fread(buffer, 1, sizeof(buffer), pstream.get())) != 0) {
    shell_output.append(buffer, chread);
  }

  return shell_output;
}

int nomad::System::exec_prog(const char **argv) {

  errno = 0;
  auto pid = fork();

  if (!pid) {
    /* Now pid is 0, so this is the cild process. */
    if (-1 == execv(argv[0], (char **)argv)) {
      throw Exception{std::string{"Failed to execute program "} + argv[0] +
                          " with error: " + strerror(errno),
                      STATUS_EXTERNAL_ERROR};
    }
    throw Exception{"Reaching this point should be impossible.",
                    STATUS_INTERNAL_ERROR};
  }

  return pid;
}

void nomad::System::empty_directory(const std::filesystem::path &dir) {

  for (const auto &entry : std::filesystem::directory_iterator(dir)) {
    errno = 0;
    if (!remove_all(entry.path())) {
      throw Exception{std::string{"Error deleting "} + entry.path().c_str() +
                          " with error: " + strerror(errno),
                      STATUS_EXTERNAL_ERROR};
    }
  }
}
