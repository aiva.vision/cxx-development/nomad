#!/bin/bash

optstring=":hv:g:u:"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -v      nomad_version\n"
  printf "    -g      gcc_version\n"
  printf "    -u      ubuntu_version, codename or release\n"
}

if [[ $# -eq 0 ]]; then
  usage
  exit 0
fi

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    v)
      nomad_version=${OPTARG}
      ;;
    g)
      gcc_version=${OPTARG}
      ;;
    u)
      ubuntu_version=${OPTARG}
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

for variable_name in "nomad_version" "gcc_version" "ubuntu_version"
do
  if [ -z ${!variable_name} ]; then
    echo "$(basename $0): Setting ${variable_name} is mandatory."
    exit 1
  fi
done

declare -a blob_storage_support

# blob_storage_support=("ON" "OFF")
blob_storage_support=("OFF")

declare -a target_stages
target_stages=("runtime" "development")

for use_blob_storage in ${blob_storage_support[@]}
do
  echo "Blob storage support is \"${use_blob_storage}\"."

  blob_label=""
  if [[ "${use_blob_storage}" == "ON" ]]; then
    blob_label=-blobs
  fi

  for target_stage in ${target_stages[@]}
  do
    gcc_label=""
    if [[ "${target_stage}" == "development" ]]; then
      gcc_label=-gcc${gcc_version}
    fi
    tag="${target_stage}-${nomad_version}${blob_label}${gcc_label}-${ubuntu_version}"

    echo "Building image with tag ${tag} ..."

    docker buildx build \
      --push \
      --platform linux/amd64,linux/arm64 \
      --cpuset-cpus $(nproc) \
      --build-arg use_blob_storage=${use_blob_storage} \
      --build-arg gcc_version=${gcc_version} \
      --build-arg ubuntu_version=${ubuntu_version} \
      --target ${target_stage} \
      -t registry.gitlab.com/aiva.vision/cxx-development/nomad:${tag} \
      -f docker/Dockerfile .
  done
done
