#pragma once

#include <memory>
#include <nlohmann/json.hpp>

#include "nomad/model.hxx"

namespace nomad {

/**
 * @brief Interprets the method and requested payload received from IoT Central.
 * An instance of this class is passed as user context for
 * device_method_callback when creating the IoT_Hub_Client_Adapter.
 */
class Method_Controller {
public:
  /**
   * @brief Constructor for the Method_Controller class.
   *
   * @param m Shared pointer to the model.
   */
  Method_Controller(const std::shared_ptr<Model> &m);

  /**
   * @brief Invoked by nomad_method_callback when a device method arrives.
   *
   * @param method_name The name of the method.
   * @param payload The payload of the method.
   * @param response The response of the method.
   * @return The status of the method execution.
   * @throws Method_Processing_Failure
   */
  int process_method(const std::string &method_name, const std::string &payload,
                     Method_Response &response);

private:
  /**
   * @brief Shared pointer to the model.
   */
  std::shared_ptr<Model> model;
};

} // namespace nomad
