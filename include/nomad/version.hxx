#pragma once

#include <string>

namespace nomad {

/**
 * @brief Retrieves the project version as a string.
 *
 * @return The project version.
 */
std::string get_project_version();

/**
 * @brief Retrieves the major part of the project version.
 *
 * @return The major version.
 */
unsigned get_project_version_major();

/**
 * @brief Retrieves the minor part of the project version.
 *
 * @return The minor version.
 */
unsigned get_project_version_minor();

/**
 * @brief Retrieves the patch part of the project version.
 *
 * @return The patch version.
 */
unsigned get_project_version_patch();

/**
 * @brief Retrieves the tweak part of the project version.
 *
 * @return The tweak version.
 */
unsigned get_project_version_tweak();

} // namespace nomad
