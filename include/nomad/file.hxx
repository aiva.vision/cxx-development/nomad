#pragma once

#include <filesystem>
namespace nomad {

namespace File {

/**
 * @brief Enum class for the status of a file.
 *
 * Check the OUTPUT section of the lsof manpage for details.
 */
enum class Status {
  not_open,
  read,
  write,
  read_write,
  unknown,
  unknown_and_lock_char
};

/**
 * @brief Checks if a file is opened by another process and returns a
 * File::Status.
 *
 * @param file The file to check.
 * @return The status of the file.
 * @throws std::invalid_argument If the file does not exist.
 */
Status status(const std::filesystem::path &file);

} // namespace File

} // namespace nomad
