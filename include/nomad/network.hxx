#pragma once

namespace nomad {

namespace Network {

enum class Status { up, down };

/**
 * @brief Checks the internet connectivity.
 *
 * @return The status of the internet connectivity.
 * @throws std::invalid_argument
 */
Status status();

} // namespace Network

} // namespace nomad
