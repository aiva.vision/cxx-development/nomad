#pragma once

#include <filesystem>
#include <string>
#include <vector>

#include <nomad/status.hxx>

namespace nomad {

/**
 * @brief Uploads a file to Azure Blob Storage.
 *
 * @param file The file to be uploaded.
 * @param container_name The name of the Azure Blob Storage container.
 * @param delete_after If set to true, the file is deleted after a successful
 * upload.
 * @param not_open If set to true, the function will not open the file.
 *
 * @throws Exception If an error occurs during the upload.
 */
void upload_file(const std::filesystem::path &file,
                 const std::string &container_name,
                 const bool delete_after = false, const bool not_open = false);

/**
 * @brief Uploads multiple files to Azure Blob Storage.
 *
 * @param source The source directory containing the files to be uploaded.
 * @param container_name The name of the Azure Blob Storage container.
 * @param empty_after If set to true, the files are deleted after a successful
 * upload.
 * @param not_open If set to true, only the files that are not accessed by
 * another process will be uploaded.
 *
 * @return The count of uploaded files.
 * @throws Exception If an error occurs during the upload.
 */
unsigned upload_files(const std::filesystem::path &source,
                      const std::string &container_name,
                      const bool empty_after = false,
                      const bool not_open = false);

/**
 * @brief Downloads a file from Azure Blob Storage.
 *
 * @param destination The destination path where the file will be downloaded.
 * @param blob_name The name of the blob to be downloaded.
 * @param container_name The name of the Azure Blob Storage container.
 *
 * @throws Exception If an error occurs during the download.
 */
void download_file(const std::filesystem::path &destination,
                   const std::string &blob_name,
                   const std::string &container_name);

/**
 * @brief Downloads multiple files from Azure Blob Storage.
 *
 * @param destination The destination directory where the files will be
 * downloaded.
 * @param blobs A vector of blob names to be downloaded.
 * @param container_name The name of the Azure Blob Storage container.
 *
 * @return The count of downloaded files.
 * @throws Exception If an error occurs during the download.
 */
unsigned download_files(const std::filesystem::path &destination,
                        const std::vector<std::string> &blobs,
                        const std::string &container_name);
} // namespace nomad
