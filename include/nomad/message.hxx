#pragma once

#include <nlohmann/json.hpp>
#include <string>

namespace nomad {

using IoT_Hub_Message = struct IOTHUB_MESSAGE_HANDLE_DATA_TAG;

/**
 * @brief Wrapper for an IoT_Hub_Message with a clearly defined serialization in
 * its constructors. The internal handle is released automatically by the
 * destructor when the object goes out of scope.
 */
class Message {
public:
  /**
   * @brief Move constructor for the Message class.
   *
   * @param m The message to move from.
   */
  Message(Message &&m);

  /**
   * @brief Initializes a message from a JSON object.
   *
   * @param data The JSON data to initialize from.
   * @throws nlohmann::detail::type_error, Serialization_Failure,
   * Property_Set_Failure
   */
  Message(const nlohmann::json &data);

  /**
   * @brief Initializes a message from a JSON object.
   *
   * @param data The JSON data to initialize from.
   * @throws nlohmann::detail::type_error, Serialization_Failure,
   * Property_Set_Failure
   */
  Message(nlohmann::json &&data);

  /**
   * @brief Initializes a message with a name and a JSON content.
   *
   * @param name The name of the message.
   * @param content The JSON content of the message.
   */
  Message(const std::string &name, const nlohmann::json &content);

  /**
   * @brief Initializes a message with a name and a JSON content.
   *
   * @param name The name of the message.
   * @param content The JSON content of the message.
   */
  Message(const std::string &name, nlohmann::json &&content);

  /**
   * @brief Destructor for the Message class. Frees the internal handle.
   */
  ~Message();

  /**
   * @brief Move assignment operator for the Message class.
   *
   * @param m The message to move from.
   * @return A reference to the moved message.
   */
  Message &operator=(Message &&m);

  /**
   * @brief Checks whether the internal data points to a valid message.
   *
   * @return True if the internal data is valid, false otherwise.
   */
  bool is_valid() const;

  /**
   * @brief Sets the message ID for the message.
   *
   * @param id The ID to set.
   * @throws Property_Set_Failure
   */
  void set_message_id(const std::string &id);

  /**
   * @brief Sets the correlation ID for the message.
   *
   * @param id The correlation ID to set.
   * @throws Property_Set_Failure
   */
  void set_correlation_id(const std::string &id);

  /**
   * @brief Sets the component name as a message property.
   *
   * @param component_name The component name to set.
   * @throws Property_Set_Failure
   */
  void set_component(const std::string &component_name);

  /**
   * @brief Returns the serialized IoT Hub message.
   *
   * @return A pointer to the IoT Hub message.
   */
  IoT_Hub_Message *get_handle() const;

private:
  /**
   * @brief Handle to a struct used by the azure-iot-c-sdk.
   */
  IoT_Hub_Message *message;
};

} // namespace nomad
