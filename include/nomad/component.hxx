#pragma once

#include <filesystem>

#include "nomad/iot_hub_client_adapter.hxx"
#include "nomad/method.hxx"
#include "nomad/method_response.hxx"
#include "nomad/property.hxx"
#include "nomad/telemetry.hxx"

namespace nomad {

/**
 * @brief Fallback name for the default component.
 */
extern const char *default_component_name;

/**
 * @brief The marker "__t" indicates that this is a component.
 */
extern const char *component_marker;

/**
 * @brief Representation of a component from the device capability model.
 * If no name is provided, it's assumed that this is the default component
 * and "default" is used as the name.
 */
class Component {
public:
  /**
   * @brief Destructor for the Component class.
   */
  virtual ~Component() {}

  /**
   * @brief Get the name of the component.
   *
   * @return The name of the component.
   */
  virtual std::string get_name() const = 0;

  /**
   * @brief Acquires a telemetry entity.
   *
   * @param telemetry The telemetry entity to acquire.
   *
   * @throws std::out_of_range If an error occurs during the acquisition.
   */
  virtual void acquire_telemetry(std::unique_ptr<Telemetry> &&telemetry) = 0;

  /**
   * @brief Iterates through the registered entities and sends them to IoT Hub.
   *
   * @throws std::out_of_range If an error occurs during the sending.
   */
  virtual void send_telemetry() const = 0;

  /**
   * @brief Manually sends a custom message that has not been registered as a
   * Telemetry at startup.
   *
   * @param name The name of the message.
   * @param content The content of the message.
   */
  virtual void send_message(const std::string &name,
                            const nlohmann::json &content) const = 0;

  /**
   * @brief Sends every read only property.
   *
   * @return True if the operation was successful, false otherwise.
   */
  virtual bool send_read_only_properties() const = 0;

  /**
   * @brief Acquires a Read_Only_Property.
   *
   * @param t The Read_Only_Property to acquire.
   *
   * @throws std::out_of_range If an error occurs during the acquisition.
   */
  virtual void
  acquire_read_only_property(std::unique_ptr<Read_Only_Property> &&t) = 0;

  /**
   * @brief Processes the twin's component after the entire payload has been
   * parsed by the controller.
   *
   * @param component The component to process.
   * @param version The version of the twin.
   */
  virtual void process_twin_component(const nlohmann::json &component,
                                      const int version) = 0;

  /**
   * @brief Processes the twin's property.
   *
   * @param name The name of the property.
   * @param content The content of the property.
   * @param version The version of the twin.
   */
  virtual void process_twin_property(const std::string &name,
                                     const nlohmann::json &content,
                                     const int version) = 0;

  /**
   * @brief Retrieves the content for a writable property.
   *
   * @param name The name of the property.
   * @return The content of the property.
   */
  virtual const nlohmann::json &
  get_writable_property(const std::string &name) const = 0;

  /**
   * @brief Insert or update a persistent property. It is meant to be used in an
   * IoT Edge deployment, where writable properties set for modules are not
   * accessible from IoT Central.
   *
   * @param name The name of the property.
   * @param content The content of the property.
   */
  virtual void set_persistent_property(const std::string &name,
                                       const nlohmann::json &content) = 0;

  /**
   * @brief Retrieves the content for a persistent property.
   *
   * @param name The name of the property.
   * @return The content of the property.
   */
  virtual const nlohmann::json &
  get_persistent_property(const std::string &name) const = 0;

  /**
   * @brief Sends a persistent property to IoT Central/Hub.
   *
   * @param name The name of the property.
   * @return True if the operation was successful, false otherwise.
   */
  virtual bool send_persistent_property(const std::string &name) const = 0;

  /**
   * @brief Sends a persistent property to IoT Central/Hub.
   *
   * @return True if the operation was successful, false otherwise.
   */
  virtual bool send_persistent_properties() const = 0;

  /**
   * @brief Reads persistent properties from disk based on the component's name.
   *
   * @param location The location of the properties on disk.
   * @throws Invalid_Location, nlohmann::detail::parse_error
   */
  virtual void
  load_persistent_properties(const std::filesystem::path &location) = 0;

  /**
   * @brief Saves persistent properties to disk based on the component's name.
   *
   * @param location The location on disk to save the properties.
   * @throws Invalid_Location, nlohmann::detail::parse_error
   */
  virtual void
  save_persistent_properties(const std::filesystem::path &location) const = 0;

  /**
   * @brief Acquires a method entity.
   *
   * @param method The method entity to acquire.
   * @throws std::out_of_range
   */
  virtual void acquire_method(std::unique_ptr<Method> &&method) = 0;

  /**
   * @brief Processes the received method initially parsed by the method
   * controller.
   *
   * @param method_name The name of the method.
   * @param payload The payload of the method.
   * @param response The response of the method.
   * @return The status of the method execution.
   * @throws Method_Processing_Failure
   */
  virtual Status execute_method(const std::string &method_name,
                                const nlohmann::json &payload,
                                Method_Response &response) = 0;

  /**
   * @brief Queries the supported methods and returns the availability of the
   * requested method.
   *
   * @param method_name The name of the method.
   * @return True if the method is available, false otherwise.
   */
  virtual bool has_method(const std::string &method_name) const = 0;
};

/**
 * @brief Implementation of the Component interface.
 */
class CComponent : public Component {
public:
  /**
   * @brief Constructor for the CComponent class.
   *
   * @param client The IoT Hub client adapter.
   * @param name The name of the component.
   */
  CComponent(const std::shared_ptr<IoT_Hub_Client_Adapter> &client,
             const std::string &name = default_component_name);

  /**
   * @brief Get the name of the component.
   *
   * @return The name of the component.
   */
  std::string get_name() const { return component_name; }

  /**
   * @brief Acquires a telemetry entity.
   *
   * @param telemetry The telemetry entity to acquire.
   */
  void acquire_telemetry(std::unique_ptr<Telemetry> &&telemetry);

  /**
   * @brief Sends the registered telemetry entities to IoT Hub.
   */
  void send_telemetry() const;

  /**
   * @brief Sends a custom message that has not been registered as a Telemetry
   * at startup.
   *
   * @param name The name of the message.
   * @param content The content of the message.
   */
  void send_message(const std::string &name,
                    const nlohmann::json &content) const;

  /**
   * @brief Acquires a Read_Only_Property.
   *
   * @param property The Read_Only_Property to acquire.
   */
  void
  acquire_read_only_property(std::unique_ptr<Read_Only_Property> &&property);

  /**
   * @brief Sends every read only property.
   *
   * @return True if the operation was successful, false otherwise.
   */
  bool send_read_only_properties() const;

  /**
   * @brief Processes the twin's component after the entire payload has been
   * parsed by the controller.
   *
   * @param component The component to process.
   * @param version The version of the twin.
   */
  void process_twin_component(const nlohmann::json &component,
                              const int version);

  /**
   * @brief Processes the twin's property.
   *
   * @param name The name of the property.
   * @param content The content of the property.
   * @param version The version of the twin.
   */
  void process_twin_property(const std::string &name,
                             const nlohmann::json &content, const int version);

  /**
   * @brief Retrieves the content for a writable property.
   *
   * @param name The name of the property.
   * @return The content of the property.
   */
  const nlohmann::json &get_writable_property(const std::string &name) const;

  /**
   * @brief Insert or update a persistent property. It is meant to be used in an
   * IoT Edge deployment, where writable properties set for modules are not
   * accessible from IoT Central.
   *
   * @param name The name of the property.
   * @param content The content of the property.
   */
  void set_persistent_property(const std::string &name,
                               const nlohmann::json &content);

  /**
   * @brief Retrieves the content for a persistent property.
   *
   * @param name The name of the property.
   * @return The content of the property.
   */
  const nlohmann::json &get_persistent_property(const std::string &name) const;

  /**
   * @brief Sends a persistent property to IoT Central/Hub.
   *
   * @param name The name of the property.
   * @return True if the operation was successful, false otherwise.
   */
  bool send_persistent_property(const std::string &name) const;

  /**
   * @brief Sends all persistent properties to IoT Central/Hub.
   *
   * @return True if the operation was successful, false otherwise.
   */
  bool send_persistent_properties() const;

  /**
   * @brief Reads persistent properties from disk based on the component's name.
   *
   * @param location The location of the properties on disk.
   */
  void load_persistent_properties(const std::filesystem::path &location);

  /**
   * @brief Saves persistent properties to disk based on the component's name.
   *
   * @param location The location on disk to save the properties.
   */
  void save_persistent_properties(const std::filesystem::path &location) const;

  /**
   * @brief Acquires a method entity.
   *
   * @param method The method entity to acquire.
   */
  void acquire_method(std::unique_ptr<Method> &&method);

  /**
   * @brief Executes the received method initially parsed by the method
   * controller.
   *
   * @param method_name The name of the method.
   * @param payload The payload of the method.
   * @param response The response of the method.
   * @return The status of the method execution.
   */
  Status execute_method(const std::string &method_name,
                        const nlohmann::json &payload,
                        Method_Response &response);

  /**
   * @brief Queries the supported methods and returns the availability of the
   * requested method.
   *
   * @param method_name The name of the method.
   * @return True if the method is available, false otherwise.
   */
  bool has_method(const std::string &method_name) const;

private:
  /**
   * @brief The name of the component, as in the device capability model.
   */
  std::string component_name;

  /**
   * @brief IoT Hub client adapter for communicating with IoT Hub.
   */
  std::shared_ptr<IoT_Hub_Client_Adapter> iot_hub_client;

  /**
   * @brief Set of supported telemetries.
   */
  std::vector<std::unique_ptr<Telemetry>> telemetries;

  /**
   * @brief Map of supported methods.
   */
  std::unordered_map<std::string, std::unique_ptr<Method>> methods;

  /**
   * @brief Device properties read and reported on startup. It is called read
   * only because it can not be modified from IoT Central.
   */
  std::vector<std::unique_ptr<Read_Only_Property>> read_only_properties;

  /**
   * @brief Properties updated from the IoT Central dashboard. These can be
   * written to from an IoT Central view for editing device and cloud
   * properties.
   */
  std::unordered_map<std::string, Writable_Property> writable_properties;

  /**
   * @brief Properties that are saved as read only in IoT Central but can be
   * modified using a command. This is necessary because IoT Central currently
   * does not support writable properties for modules. These properties can be
   * sent as read only properties.
   */
  std::unordered_map<std::string, Persistent_Property> persistent_properties;
};

} // namespace nomad
