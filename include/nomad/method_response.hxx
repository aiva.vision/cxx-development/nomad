#pragma once

#include <nlohmann/json.hpp>

namespace nomad {

/**
 * @brief Ensures that the lower level response adheres to the standard even if
 * not set. It's used by the method controller. The response is eventually owned
 * by the azure-iot-sdk-c, so the passed responses are duplicated using
 * strdup().
 *
 * Refer to this for memory ownership:
 * https://github.com/Azure/iotedge/issues/2995
 */
class Method_Response {
public:
  /**
   * @brief Constructor for the Method_Response class.
   *
   * @param response Pointer to the response data.
   * @param response_size Pointer to the size of the response data.
   */
  Method_Response(unsigned char **response, size_t *response_size);

  /**
   * @brief Destructor for the Method_Response class.
   * Sets the response to an empty json if the response hasn't been set already.
   */
  ~Method_Response();

  /**
   * @brief Sets the response and the response size from the content.
   * In addition, it allocates a new response and copies the content. If the
   * response is not null, it is freed and a new one is allocated. The new
   * response is owned by the azure-iot-sdk-c.
   *
   * @param content The content to set as the response.
   */
  void set(const nlohmann::json &content);

private:
  /**
   * @brief Pointer to the response data.
   */
  unsigned char **response;

  /**
   * @brief Pointer to the size of the response data.
   */
  size_t *response_size;
};

} // namespace nomad
