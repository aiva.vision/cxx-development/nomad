#pragma once

#include <nlohmann/json.hpp>
#include <unordered_map>

#include "nomad/component.hxx"
#include "nomad/iot_hub_client_adapter.hxx"
#include "nomad/method_response.hxx"
#include "nomad/status.hxx"

namespace nomad {

/**
 * @brief Leverages the Composite pattern and unifies the telemetry, properties
 * and methods in components, which are later unified under the model.
 */
class Model {
public:
  /**
   * @brief Virtual destructor for the Model class.
   */
  virtual ~Model() {}

  /**
   * @brief Registers a component and assumes ownership for it.
   *
   * @param component The component to acquire.
   */
  virtual void acquire_component(std::unique_ptr<Component> &&component) = 0;

  /**
   * @brief Checks whether a named component has been registered in the model.
   *
   * @param name The name of the component.
   * @return True if the component exists, false otherwise.
   */
  virtual bool has_component(const std::string &name) const = 0;

  /**
   * @brief Uses the iterator pattern to ask the components to send their
   * registered Telemetries.
   */
  virtual void send_telemetry() const = 0;

  /**
   * @brief Sends an individual message as telemetry. Recurrent and predictable
   * telemetry should be registered as Telemetry entities for specific
   * components.
   *
   * @param name The name of the message.
   * @param content The content of the message.
   * @param component The component of the message.
   */
  virtual void
  send_message(const std::string &name, const nlohmann::json &content,
               const std::string &component = default_component_name) const = 0;

  /**
   * @brief Uses the iterator pattern to ask the components to send their
   * registered read only properties.
   */
  virtual void send_read_only_properties() const = 0;

  /**
   * @brief Reads persistent properties from disk for every component.
   *
   * @param location The location of the properties.
   * @throws Invalid_Location, nlohmann::detail::parse_error
   */
  virtual void
  load_persistent_properties(const std::filesystem::path &location) = 0;

  /**
   * @brief Saves persistent properties to disk for every component.
   *
   * @param location The location to save the properties.
   * @throws Invalid_Location, nlohmann::detail::parse_error
   */
  virtual void
  save_persistent_properties(const std::filesystem::path &location) const = 0;

  /**
   * @brief Uses the iterator pattern to ask the components to send their
   * persistent properties.
   */
  virtual void send_persistent_properties() const = 0;

  /**
   * @brief Retrieves the content for a persistent property.
   *
   * @param name The name of the property.
   * @param component The component of the property.
   * @return The content of the property.
   */
  virtual const nlohmann::json &get_persistent_property(
      const std::string &name,
      const std::string &component = default_component_name) const = 0;

  /**
   * @brief Sets the content for a persistent property.
   *
   * @param name The name of the property.
   * @param value The value to set for the property.
   * @param component The component of the property.
   */
  virtual void set_persistent_property(
      const std::string &name, const nlohmann::json &value,
      const std::string &component = default_component_name) = 0;

  /**
   * @brief Processes the received method initially parsed by the method
   * controller.
   *
   * @param method_name The name of the method.
   * @param component_name The name of the component.
   * @param payload The payload for the method.
   * @param response The response for the method.
   * @return The status of the method execution.
   * @throws Method_Processing_Failure
   */
  virtual Status process_method(const std::string &method_name,
                                const std::string &component_name,
                                const nlohmann::json &payload,
                                Method_Response &response) = 0;

  /**
   * @brief Processes the twin parsed initially received and parsed by the
   * Twin_Controller.
   *
   * @param data The twin data to process.
   * @throws Twin_Processing_Failure
   */
  virtual void process_twin_data(const nlohmann::json &data) = 0;

  /**
   * @brief Calls do_work on the ioot_hub_client.
   */
  virtual void do_work() noexcept = 0;
};

/**
 * @brief Implementation of the Model interface.
 */
class CModel : public Model {
public:
  /**
   * @brief Constructor for the CModel class.
   *
   * @param client Shared pointer to the IoT_Hub_Client_Adapter.
   */
  CModel(const std::shared_ptr<IoT_Hub_Client_Adapter> &client);

  /**
   * @brief Registers a component and assumes ownership for it.
   *
   * @param component The component to acquire.
   */
  void acquire_component(std::unique_ptr<Component> &&component);

  /**
   * @brief Checks whether a named component has been registered in the model.
   *
   * @param name The name of the component.
   * @return True if the component exists, false otherwise.
   */
  bool has_component(const std::string &name) const;

  /**
   * @brief Uses the iterator pattern to ask the components to send their
   * registered Telemetries.
   */
  void send_telemetry() const;

  /**
   * @brief Sends an individual message as telemetry. Recurrent and predictable
   * telemetry should be registered as Telemetry entities for specific
   * components.
   *
   * @param name The name of the message.
   * @param content The content of the message.
   * @param component The component of the message.
   */
  void
  send_message(const std::string &name, const nlohmann::json &content,
               const std::string &component = default_component_name) const;

  /**
   * @brief Uses the iterator pattern to ask the components to send their
   * registered read only properties.
   */
  void send_read_only_properties() const;

  /**
   * @brief Reads persistent properties from disk for every component.
   *
   * @param location The location of the properties.
   * @throws Invalid_Location, nlohmann::detail::parse_error
   */
  void load_persistent_properties(const std::filesystem::path &location);

  /**
   * @brief Saves persistent properties to disk for every component.
   *
   * @param location The location to save the properties.
   * @throws Invalid_Location, nlohmann::detail::parse_error
   */
  void save_persistent_properties(const std::filesystem::path &location) const;

  /**
   * @brief Uses the iterator pattern to ask the components to send their
   * persistent properties.
   */
  void send_persistent_properties() const;

  /**
   * @brief Retrieves the content for a persistent property.
   *
   * @param name The name of the property.
   * @param component The component of the property.
   * @return The content of the property.
   */
  const nlohmann::json &get_persistent_property(
      const std::string &name,
      const std::string &component = default_component_name) const;

  /**
   * @brief Sets the content for a persistent property.
   *
   * @param name The name of the property.
   * @param value The value to set for the property.
   * @param component The component of the property.
   */
  void set_persistent_property(
      const std::string &name, const nlohmann::json &value,
      const std::string &component = default_component_name);

  /**
   * @brief Processes the received method initially parsed by the method
   * controller.
   *
   * @param method_name The name of the method.
   * @param component_name The name of the component.
   * @param payload The payload for the method.
   * @param response The response for the method.
   * @return The status of the method execution.
   * @throws Method_Processing_Failure
   */
  Status process_method(const std::string &method_name,
                        const std::string &component_name,
                        const nlohmann::json &payload,
                        Method_Response &response);

  /**
   * @brief Processes the twin parsed initially received and parsed by the
   * Twin_Controller.
   *
   * @param data The twin data to process.
   * @throws Twin_Processing_Failure
   */
  void process_twin_data(const nlohmann::json &data);

  /**
   * @brief Calls do_work on the ioot_hub_client.
   */
  void do_work() noexcept;

private:
  /**
   * @brief Shared pointer to the IoT_Hub_Client_Adapter.
   */
  std::shared_ptr<IoT_Hub_Client_Adapter> iot_hub_client;

  /**
   * @brief Map of components supporting writable properties and therefore
   * device twins. These properties are resynced on every startup, so there is
   * no need to store them on the disk like the Edge_Components do.
   */
  std::unordered_map<std::string, std::unique_ptr<Component>> components;
};

} // namespace nomad
