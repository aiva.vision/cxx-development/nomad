#pragma once

#include <exception>
#include <string>

#include "nomad/status.hxx"

namespace nomad {

/**
 * @brief Base exception class.
 */
class Exception : public std::exception {
public:
  /**
   * @brief Constructor for the Exception class.
   * 
   * @param m The exception message.
   * @param s The status of the exception.
   */
  Exception(const std::string &m, Status s);

  /**
   * @brief Retrieves the exception message.
   * 
   * @return The exception message.
   */
  const char *what() const noexcept;

  /**
   * @brief Retrieves the status of the exception.
   * 
   * @return The status of the exception.
   */
  Status get_status() const noexcept;

private:
  /**
   * @brief The exception message.
   */
  std::string message;

  /**
   * @brief The status of the exception.
   */
  Status status;
};

/**
 * @brief Exception class for provisioning errors.
 */
class Provisioning_Error : public Exception {
public:
  /**
   * @brief Constructor for the Provisioning_Error class.
   * 
   * @param m The exception message.
   */
  Provisioning_Error(const std::string &m);
};

/**
 * @brief Exception class for unset environment variables.
 */
class Unset_Env_Var : public Exception {
public:
  /**
   * @brief Constructor for the Unset_Env_Var class.
   * 
   * @param name The name of the unset environment variable.
   */
  Unset_Env_Var(const std::string &name);
};

/**
 * @brief Exception class for method processing errors.
 */
class Method_Processing_Error : public Exception {
public:
  /**
   * @brief Constructor for the Method_Processing_Error class.
   * 
   * @param method The name of the method that caused the error.
   * @param message The exception message.
   * @param s The status of the exception.
   */
  Method_Processing_Error(const std::string &method, const std::string &message,
                          Status s);
};

/**
 * @brief Exception class for twin processing errors.
 */
class Twin_Processing_Error : public Exception {
public:
  /**
   * @brief Constructor for the Twin_Processing_Error class.
   * 
   * @param m The exception message.
   * @param s The status of the exception.
   */
  Twin_Processing_Error(const std::string &m, Status s);
};

/**
 * @brief Exception class for twin component processing errors.
 */
class Twin_Component_Processing_Error : public Exception {
public:
  /**
   * @brief Constructor for the Twin_Component_Processing_Error class.
   * 
   * @param component The name of the component that caused the error.
   * @param message The exception message.
   * @param s The status of the exception.
   */
  Twin_Component_Processing_Error(const std::string &component,
                                  const std::string &message, Status s);
};

/**
 * @brief Exception class for invalid locations.
 */
class Invalid_Location : public Exception {
public:
  /**
   * @brief Constructor for the Invalid_Location class.
   * 
   * @param location The invalid location.
   * @param message The exception message.
   */
  Invalid_Location(const std::string &location, const std::string &message);
};

/**
 * @brief Exception class for IoT Hub client errors.
 */
class IoT_Hub_Client_Error : public Exception {
public:
  /**
   * @brief Constructor for the IoT_Hub_Client_Error class.
   * 
   * @param m The exception message.
   * @param s The status of the exception.
   */
  IoT_Hub_Client_Error(const std::string &m, Status s);
};

/**
 * @brief Exception class for option set errors.
 */
class Option_Set_Error : public IoT_Hub_Client_Error {
public:
  /**
   * @brief Constructor for the Option_Set_Error class.
   * 
   * @param name The name of the option that caused the error.
   * @param result The result of the operation.
   */
  Option_Set_Error(const std::string &name, const int &result);
};

/**
 * @brief Exception class for unsupported connection types.
 */
class Unsupported_Connection_Type : public IoT_Hub_Client_Error {
public:
  /**
   * @brief Constructor for the Unsupported_Connection_Type class.
   * 
   * @param connection_type The unsupported connection type.
   */
  Unsupported_Connection_Type(const std::string &connection_type);
};

/**
 * @brief Exception class for property set errors.
 */
class Property_Set_Error : public Exception {
public:
  /**
   * @brief Constructor for the Property_Set_Error class.
   * 
   * @param name The name of the property that caused the error.
   * @param res The result of the operation.
   */
  Property_Set_Error(const std::string &name, const int &res);
};

/**
 * @brief Exception class for message creation errors.
 */
class Message_Create_Error : public Exception {
public:
  /**
   * @brief Constructor for the Message_Create_Error class.
   */
  Message_Create_Error();
};

} // namespace nomad
