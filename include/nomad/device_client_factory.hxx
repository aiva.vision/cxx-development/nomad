#pragma once

#include <exception>
#include <string>

namespace nomad {

using IoT_Hub_LL_Device_Client = struct IOTHUB_CLIENT_CORE_LL_HANDLE_DATA_TAG;

/**
 * @brief Environment variable for the device template ID.
 */
extern const char *device_template_id_env_var;

/**
 * @brief Enum class for the state of the DPS registration process.
 * We cannot proceed with PnP until we get into the state
 * Registration_Succeeded.
 */
enum class DPS_Registration_Status {
  Registration_Not_Complete,
  Registration_Succeeded,
  Registration_Failed
};

/**
 * @brief Checks whether the IOTHUB_ENABLE_TRACING is set to true or false.
 *
 * @return True if tracing is enabled, false otherwise.
 */
bool is_tracing_enabled();

/**
 * @brief Interface for device client factories.
 */
class LL_Device_Client_Factory {
public:
  virtual ~LL_Device_Client_Factory() {}

  /**
   * @brief Creates a new low-level device client.
   *
   * @return A pointer to the new low-level device client.
   */
  virtual IoT_Hub_LL_Device_Client *create_ll_client() = 0;
};

/**
 * @brief Builds a device client using the Device Provisioning Service.
 */
class DPS_LL_Device_Client_Factory : public LL_Device_Client_Factory {
public:
  /**
   * @brief Builds a DPS factory using the environment.
   *
   * @throws Provisioning_Error
   */
  DPS_LL_Device_Client_Factory();

  /**
   * @brief Destructor for the DPS_LL_Device_Client_Factory class.
   */
  ~DPS_LL_Device_Client_Factory();

  /**
   * @brief Updates the registration status in the callback.
   *
   * @param status The new registration status.
   */
  void set_registration_status(DPS_Registration_Status status);

  /**
   * @brief Sets the generated IoT Hub uri received in the callback.
   *
   * @param uri The IoT Hub URI.
   * @return True if the operation was successful, false otherwise.
   */
  bool set_iothub_uri(const char *uri);

  /**
   * @brief Allocates and registers the low level client.
   *
   * @throws Provisioning_Error
   * @return A pointer to the new low-level device client.
   */
  IoT_Hub_LL_Device_Client *create_ll_client();

private:
  /**
   * @brief Registers the device using the provisioning client before attempting
   * to create the device client.
   *
   * @throws Provisioning_Error
   */
  void register_device();

  /**
   * @brief Flag for enabling the verbose output of the provisioning client.
   */
  bool enable_tracing;

  /**
   * @brief ID of existing template uploaded in IoT Central.
   */
  const char *template_id;

  /**
   * @brief Endpoint for provisioning.
   */
  const char *endpoint;

  /**
   * @brief Scope ID from IoT Central.
   */
  const char *scope_id;

  /**
   * @brief DeviceId for this device as determined by the DPS client runtime.
   */
  const char *device_id;

  /**
   * @brief Used for authentication.
   */
  const char *device_key;

  /**
   * @brief IoT Hub for this device as determined by the DPS client runtime.
   */
  char *iothub_uri;

  /**
   * @brief Registration status, modified by provisioning_register_callback.
   */
  DPS_Registration_Status registration_status;
};

/**
 * @brief Builds a device client using a Device Connection String.
 */
class DCS_LL_Device_Client_Factory : public LL_Device_Client_Factory {
public:
  /**
   * @brief Builds a DCS factory using the environment.
   *
   * @throws Unset_Env_Var
   */
  DCS_LL_Device_Client_Factory();

  /**
   * @brief Allocates the low level client client.
   *
   * @return A pointer to the new low-level device client.
   */
  IoT_Hub_LL_Device_Client *create_ll_client();

private:
  /**
   * @brief Device Connection String used when creating the low level client.
   */
  const char *device_conn_string;
};

} // namespace nomad
