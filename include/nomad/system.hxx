#include <filesystem>
#include <string>

namespace nomad {

namespace System {

/**
 * @brief Returns the value of the environment variable or throws Unset_Env_Var
 * if it's not found.
 *
 * @param name The name of the environment variable.
 * @return The value of the environment variable.
 * @throws Unset_Env_Var
 */
const char *env_var(const char *name);

/**
 * @brief Returns a timestamp based on the system's current time.
 *
 * @return The current timestamp.
 */
std::string timestamp();

/**
 * @brief Executes a shell command and returns the output. The standard error is
 * automatically redirected to the standard output.
 *
 * @param command The command to execute.
 * @return The output of the command.
 */
std::string exec_shell(const std::string &command);

/**
 * @brief Executes a program and returns the process id on success or -1 on
 * error.
 *
 * @param argv The arguments for the program.
 * @return The process id on success or -1 on error.
 * @throws std::runtime_error
 */
int exec_prog(const char **argv);

/**
 * @brief Removes all of the files and subdirectories for a directory.
 *
 * @param dir The directory to empty.
 * @throws std::runtime_error
 */
void empty_directory(const std::filesystem::path &dir);

} // namespace System

} // namespace nomad
