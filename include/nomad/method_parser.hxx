#pragma once

#include <string>

namespace nomad {

/**
 * @brief Parses the received method and provides a convenient way to access the
 * method's name and the component's name.
 */
class Method_Parser {
public:
  /**
   * @brief Constructor for the Method_Parser class.
   *
   * @param received_method The received method to parse.
   */
  Method_Parser(const std::string &received_method);

  /**
   * @brief Returns the parsed method name.
   *
   * @return The method name.
   */
  std::string &get_method();

  /**
   * @brief Returns the parsed component name.
   *
   * @return The component name.
   */
  std::string &get_component();

private:
  /**
   * @brief The parsed method name.
   */
  std::string method;

  /**
   * @brief The parsed component name.
   */
  std::string component;
};

} // namespace nomad
