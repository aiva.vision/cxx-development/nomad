#pragma once

#include <memory>
#include <nlohmann/json.hpp>

#include "nomad/model.hxx"

namespace nomad {

enum class Twin_Update_State { complete, partial };

/**
 * @brief Handles the device twins sent by IoT Central.
 */
class Twin_Controller {
public:
  /**
   * @brief Constructor for the Twin_Controller class.
   *
   * @param model Shared pointer to the Model.
   */
  Twin_Controller(std::shared_ptr<Model> model);

  /**
   * @brief Invoked by the application when a device twin arrives to its device
   * twin processing callback.
   *
   * @param state The state of the twin update.
   * @param payload The payload of the twin update.
   * @throws Twin_Processing_Failure, nlohmann::detail::parse_error
   */
  void process_received_twin(Twin_Update_State state,
                             const std::string &payload);

  /**
   * @brief Retrieves the json object corresponding to the desired payload.
   *
   * @param state The state of the twin update.
   * @param payload The payload of the twin update.
   * @return The desired object from the payload.
   */
  nlohmann::json get_desired_object(Twin_Update_State state,
                                    const std::string &payload);

private:
  /**
   * @brief Shared pointer to the Model.
   */
  std::shared_ptr<Model> model;
};

} // namespace nomad
