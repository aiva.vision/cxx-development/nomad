#pragma once

namespace nomad {

/**
 * @brief Status codes for nomad, closely mapping to HTTP status.
 */
enum Status {
  STATUS_SUCCESS = 200,
  STATUS_PENDING = 202,
  STATUS_ASYNC_TASK_NOT_READY = 203,
  STATUS_BAD_FORMAT = 400,
  STATUS_INTERNAL_ERROR = 401,
  STATUS_EXTERNAL_ERROR = 402,
  STATUS_NOT_FOUND = 404,
};

} // namespace nomad
