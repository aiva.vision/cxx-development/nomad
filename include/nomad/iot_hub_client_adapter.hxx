#pragma once

#include <string>

#include "nomad/device_client_factory.hxx"
#include "nomad/message.hxx"

namespace nomad {

using IoT_Hub_LL_Module_Client = struct IOTHUB_MODULE_CLIENT_LL_HANDLE_DATA_TAG;
using IoT_Hub_LL_Device_Client = struct IOTHUB_CLIENT_CORE_LL_HANDLE_DATA_TAG;

class Method_Controller;
class Twin_Controller;

/**
 * @brief Telemetry name for warning messages.
 */
extern const char *warning_telemetry_name;

/**
 * @brief Telemetry name for error messages.
 */
extern const char *error_telemetry_name;

/**
 * @brief Adapter class for the IoT Hub Client.
 */
class IoT_Hub_Client_Adapter {
public:
  /**
   * @brief Destructor for the IoT_Hub_Client_Adapter class.
   */
  virtual ~IoT_Hub_Client_Adapter(){};

  /**
   * @brief Optionally, set the Method Controller that processes incoming device
   * methods, using a dummy callback function, which is the channel PnP Commands
   * are transferred over.
   *
   * @param controller Pointer to the Method Controller.
   */
  virtual void set_method_controller(Method_Controller *controller) = 0;

  /**
   * @brief Optionally, set the Twin Controller that processes device twin
   * changes from the IoTHub using a dummy callback function, which is the
   * channel that writable properties are transferred over.  This will also
   * automatically retrieve the full twin for the application on startup.
   *
   * @param controller Pointer to the Twin Controller.
   */
  virtual void set_twin_controller(Twin_Controller *controller) = 0;

  /**
   * @brief Sends the serialized message from the wrapper.
   *
   * @param message The message to send.
   */
  virtual void send_message(const Message &message) const = 0;

  /**
   * @brief Sends a serialized property to IoT Central/Hub. Using a handle is
   * not possible because the serialization methods for read only and writable
   * properties require different parameters.
   *
   * @param serialized_property The serialized property to send.
   * @return True if the operation was successful, false otherwise.
   */
  virtual bool
  send_property(const std::string &serialized_property) const noexcept = 0;

  /**
   * @brief Wake up periodically to poll.  Even if we do not plan on sending
   * telemetry, we still need to poll periodically in order to process
   * incoming requests from the server and to do connection keep alives.
   */
  virtual void do_work() noexcept = 0;
};

/**
 * @brief IoT Hub client adapter that holds a low level device client.
 */
class Device_Client_Adapter : public IoT_Hub_Client_Adapter {
public:
  /**
   * @brief Move constructor for the Device_Client_Adapter class.
   *
   * @param client The client to move from.
   */
  Device_Client_Adapter(Device_Client_Adapter &&client);

  /**
   * @brief Constructor for the Device_Client_Adapter class.
   *
   * @param factory The factory to use for creating the low level device client.
   */
  Device_Client_Adapter(LL_Device_Client_Factory &factory);

  /**
   * @brief Destructor for the Device_Client_Adapter class.
   */
  ~Device_Client_Adapter();

  /**
   * @brief Sets the Method Controller that processes incoming device methods.
   *
   * @param controller Pointer to the Method Controller.
   */
  void set_method_controller(Method_Controller *controller);

  /**
   * @brief Sets the Twin Controller that processes device twin changes.
   *
   * @param controller Pointer to the Twin Controller.
   */
  void set_twin_controller(Twin_Controller *controller);

  /**
   * @brief Sends the specified message.
   *
   * @param message The message to send.
   */
  void send_message(const Message &message) const;

  /**
   * @brief Sends a serialized property.
   *
   * @param serialized_property The serialized property to send.
   * @return True if the operation was successful, false otherwise.
   */
  bool send_property(const std::string &serialized_property) const noexcept;

  /**
   * @brief Performs periodic tasks.
   */
  void do_work() noexcept;

private:
  /**
   * @brief Pointer to the low level device client.
   */
  IoT_Hub_LL_Device_Client *ll_device_client;
};

/**
 * @brief IoT Hub client adapter that holds a low level module client.
 */
class Module_Client_Adapter : public IoT_Hub_Client_Adapter {
public:
  /**
   * @brief Default constructor for the Module_Client_Adapter class.
   */
  Module_Client_Adapter();

  /**
   * @brief Move constructor for the Module_Client_Adapter class.
   *
   * @param client The client to move from.
   */
  Module_Client_Adapter(Module_Client_Adapter &&client);

  /**
   * @brief Destructor for the Module_Client_Adapter class.
   */
  ~Module_Client_Adapter();

  /**
   * @brief Sets the Method Controller that processes incoming device methods.
   *
   * @param controller Pointer to the Method Controller.
   */
  void set_method_controller(Method_Controller *controller);

  /**
   * @brief Sets the Twin Controller that processes device twin changes.
   *
   * @param controller Pointer to the Twin Controller.
   */
  void set_twin_controller(Twin_Controller *controller);

  /**
   * @brief Sends the specified message.
   *
   * @param message The message to send.
   */
  void send_message(const Message &message) const;

  /**
   * @brief Sends a serialized property.
   *
   * @param serialized_property The serialized property to send.
   * @return True if the operation was successful, false otherwise.
   */
  bool send_property(const std::string &serialized_property) const noexcept;

  /**
   * @brief Performs periodic tasks.
   */
  void do_work() noexcept;

private:
  /**
   * @brief Pointer to the low level module client.
   */
  IoT_Hub_LL_Module_Client *ll_module_client;
};

/**
 * @brief Creates an IoT Hub client based on the environment configuration.
 *
 * @return A shared pointer to the created IoT Hub client.
 */
std::shared_ptr<IoT_Hub_Client_Adapter> create_iot_hub_client();

} // namespace nomad
