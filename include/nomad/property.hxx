#pragma once

#include <memory>

#include "nomad/iot_hub_client_adapter.hxx"
#include "nomad/status.hxx"

namespace nomad {

/**
 * @brief Uses the Template Method Pattern and allows the user to define custom
 * read only properties by implementing get_property_content().
 */
class Read_Only_Property {
public:
  /**
   * @brief Constructor for the Read_Only_Property class.
   *
   * @param name The name of the property.
   * @param client Shared pointer to the IoT_Hub_Client_Adapter.
   */
  Read_Only_Property(const std::string &name,
                     const std::shared_ptr<IoT_Hub_Client_Adapter> &client);

  /**
   * @brief Virtual destructor for the Read_Only_Property class.
   */
  virtual ~Read_Only_Property() {}

  /**
   * @brief Sends the property.
   *
   * @param component The component to send.
   * @return True if the property was sent successfully, false otherwise.
   */
  bool send(const std::string &component) const;

  /**
   * @brief Retrieves the name of the property.
   *
   * @return The name of the property.
   */
  const std::string &get_name() const;

  /**
   * @brief Retrieves the property's content.
   *
   * @return The content of the property.
   */
  virtual nlohmann::json get_property_content() const = 0;

private:
  /**
   * @brief The name of the property.
   */
  std::string property_name;

  /**
   * @brief Shared pointer to the IoT_Hub_Client_Adapter.
   */
  std::shared_ptr<IoT_Hub_Client_Adapter> iot_hub_client;
};

/**
 * @brief Holds its content and allows setting it during its lifetime.
 */
class Writable_Property {
public:
  /**
   * @brief Move constructor for the Writable_Property class.
   *
   * @param property The property to move.
   */
  Writable_Property(Writable_Property &&property);

  /**
   * @brief Constructor for the Writable_Property class.
   *
   * @param name The name of the property.
   * @param content The content of the property.
   * @param client Shared pointer to the IoT_Hub_Client_Adapter.
   */
  Writable_Property(const std::string &name, const nlohmann::json &content,
                    const std::shared_ptr<IoT_Hub_Client_Adapter> &client);

  /**
   * @brief Move assignment operator for the Writable_Property class.
   *
   * @param property The property to move.
   * @return Reference to the moved property.
   */
  Writable_Property &operator=(Writable_Property &&property);

  /**
   * @brief Destructor for the Writable_Property class.
   */
  ~Writable_Property() {}

  /**
   * @brief Sets the content for the property.
   *
   * @param content The content to set.
   */
  void set_content(const nlohmann::json &content);

  /**
   * @brief Retrieves the content of the property.
   *
   * @return The content of the property.
   */
  const nlohmann::json &get_content() const;

  /**
   * @brief Sends the property.
   *
   * @param component The component to send.
   * @param status The status of the property.
   * @param description The description of the property.
   * @param version The version of the property.
   * @return True if the property was sent successfully, false otherwise.
   */
  bool send(const std::string &component, const Status status,
            const std::string &description, const int version) const;

private:
  /**
   * @brief The name of the property.
   */
  std::string property_name;

  /**
   * @brief The content of the property.
   */
  nlohmann::json property_content;

  /**
   * @brief Shared pointer to the IoT_Hub_Client_Adapter.
   */
  std::shared_ptr<IoT_Hub_Client_Adapter> iot_hub_client;
};

/**
 * @brief Holds its content and allows setting it during its lifetime.
 */
class Persistent_Property {
public:
  /**
   * @brief Move constructor for the Persistent_Property class.
   *
   * @param property The property to move.
   */
  Persistent_Property(Persistent_Property &&property);

  /**
   * @brief Constructor for the Persistent_Property class.
   *
   * @param name The name of the property.
   * @param content The content of the property.
   * @param client Shared pointer to the IoT_Hub_Client_Adapter.
   */
  Persistent_Property(const std::string &name, const nlohmann::json &content,
                      const std::shared_ptr<IoT_Hub_Client_Adapter> &client);

  /**
   * @brief Move assignment operator for the Persistent_Property class.
   *
   * @param property The property to move.
   * @return Reference to the moved property.
   */
  Persistent_Property &operator=(Persistent_Property &&property);

  /**
   * @brief Destructor for the Persistent_Property class.
   */
  ~Persistent_Property() {}

  /**
   * @brief Sets the content for the property.
   *
   * @param content The content to set.
   */
  void set_content(const nlohmann::json &content);

  /**
   * @brief Retrieves the content of the property.
   *
   * @return The content of the property.
   */
  const nlohmann::json &get_content() const;

  /**
   * @brief Sends the property.
   *
   * @param component The component to send.
   * @return True if the property was sent successfully, false otherwise.
   */
  bool send(const std::string &component) const;

private:
  /**
   * @brief The name of the property.
   */
  std::string property_name;

  /**
   * @brief The content of the property.
   */
  nlohmann::json property_content;

  /**
   * @brief Shared pointer to the IoT_Hub_Client_Adapter.
   */
  std::shared_ptr<IoT_Hub_Client_Adapter> iot_hub_client;
};

/**
 * @brief Supervises the property update process and reports the final
 * status when going out of scope. It is especially useful for
 * sending the reported property even when exceptions are thrown.
 */
class Property_Reporter {
public:
  /**
   * @brief Constructor for the Property_Reporter class.
   *
   * @param name The name of the property.
   * @param component The component of the property.
   * @param ver The version of the property.
   * @param client Shared pointer to the IoT_Hub_Client_Adapter.
   */
  Property_Reporter(const std::string &name, const std::string &component,
                    const int ver,
                    const std::shared_ptr<IoT_Hub_Client_Adapter> &client);

  /**
   * @brief Destructor for the Property_Reporter class. Calls send_report()
   * automatically.
   */
  ~Property_Reporter();

  /**
   * @brief Updates the current saved status.
   *
   * @param content The content to set.
   */
  void set_content(const nlohmann::json &content);

  /**
   * @brief Updates the current saved status.
   *
   * @param status The status to set.
   */
  void set_status(const Status status);

private:
  /**
   * @brief Called by the destructor to send the reported property to IoT
   * Central/Hub.
   */
  void send_report() const;

  /**
   * @brief The last value held by the property.
   */
  std::string property_name;

  /**
   * @brief The last value held by the property.
   */
  nlohmann::json property_content;

  /**
   * @brief The current component's name.
   */
  std::string component;

  /**
   * @brief The last available status before an exception has been thrown.
   */
  Status status;

  /**
   * @brief The property version received from IoT Central/Hub.
   */
  int version;

  /**
   * @brief IoT Hub Client Adapter.
   */
  std::shared_ptr<IoT_Hub_Client_Adapter> iot_hub_client;
};

/**
 * @brief Serializes a read only property according to the component's name.
 *
 * @param name The name of the property.
 * @param content The content of the property.
 * @param component The name of the component.
 * @return The serialized property.
 * @throws nlohmann::detail::type_error
 */
std::string serialize_read_only_property(const std::string &name,
                                         const nlohmann::json &content,
                                         const std::string &component);
} // namespace nomad
