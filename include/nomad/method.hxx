#pragma once

#include <nlohmann/json.hpp>

#include "nomad/method_response.hxx"
#include "nomad/status.hxx"

namespace nomad {

class Component;

/**
 * @brief Functor representing a method received from IoT Central.
 *
 * This serves as a strategy for the method controller.
 */
class Method {
public:
  /**
   * @brief Constructor for the Method class.
   *
   * @param name The name of the method.
   */
  Method(const std::string &name) : method_name{name} {}

  /**
   * @brief Virtual destructor for the Method class.
   */
  virtual ~Method() {}

  /**
   * @brief Returns the name of the method.
   *
   * @return The name of the method.
   */
  const std::string &get_name() const { return method_name; }

  /**
   * @brief Executes the method with the given payload and response.
   *
   * @param payload The payload for the method.
   * @param response The response for the method.
   * @param component The component for the method.
   * @return The status of the method execution.
   * @throws Method_Processing_Failure
   */
  virtual Status execute(const nlohmann::json &payload,
                         Method_Response &response, Component *component) = 0;

private:
  /**
   * @brief The name of the method.
   */
  std::string method_name;
};

} // namespace nomad
