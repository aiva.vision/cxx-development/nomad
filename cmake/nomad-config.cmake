include(CMakeFindDependencyMacro)
find_dependency(Threads)
find_dependency(doctest)
find_dependency(trompeloeil)

include(${CMAKE_CURRENT_LIST_DIR}/nomad_export.cmake)
