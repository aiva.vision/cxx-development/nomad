message(STATUS "In AzureIoT Find module.")

list(APPEND CMAKE_MESSAGE_INDENT "  ")

if(use_prov_client)
  #The "run_e2e_tests" must be set to OFF because otherwise the e2e's test HSM
  #layer and symmetric key logic will conflict.
  set(hsm_type_symm_key ON CACHE BOOL "" FORCE)
  set(run_e2e_tests OFF CACHE BOOL "" FORCE)
endif()

set(AzureIoT_INCLUDE_DIRS
  ${IOTHUB_CLIENT_INC_FOLDER}
  ${SHARED_UTIL_INC_FOLDER}
  ${UMOCK_C_INC_FOLDER}
  ${azure_iot_sdks_SOURCE_DIR}/deps/parson
  ${MACRO_UTILS_INC_FOLDER}
  ${DEV_AUTH_MODULES_CLIENT_INC_FOLDER})

set(AzureIoT_LIBRARIES
  iothub_client
  parson)

if(use_blob_storage)
  list(APPEND AzureIoT_LIBRARIES azure-storage-blobs)
  add_compile_definitions(HAVE_BLOB_STORAGE)
endif()

if (use_edge_modules)
  add_compile_definitions(USE_EDGE_MODULES)
  message(STATUS "Added compile definition for edge modules.")
endif()

if (use_mqtt)
  add_compile_definitions(USE_MQTT)
  list(APPEND AzureIoT_LIBRARIES
    iothub_client_mqtt_transport
    iothub_client_mqtt_ws_transport
    umqtt)

  list(APPEND AzureIoT_INCLUDE_DIRS
    ${MQTT_INC_FOLDER})

  message(STATUS "Added compile definition, libraries and include directories for MQTT.")
endif()

if (use_amqp)
  add_compile_definitions(USE_AMQP)

  list(APPEND AzureIoT_LIBRARIES
    iothub_client_amqp_transport
    iothub_client_amqp_ws_transport
    uamqp)

  list(APPEND AzureIoT_INCLUDE_DIRS
    ${AMQP_INC_FOLDER})

  message(STATUS "Added compile definition, libraries and include directories for AMQP.")
endif()

if (use_prov_client)
  add_compile_definitions(USE_PROV_MODULE)
  if(use_mqtt)
    list(APPEND AzureIoT_LIBRARIES
    prov_mqtt_transport
    prov_mqtt_ws_transport)
  endif()

  if(use_amqp)
    list(APPEND AzureIoT_LIBRARIES
    prov_amqp_transport
    prov_amqp_ws_transport)
  endif()

  list(APPEND AzureIoT_LIBRARIES
    prov_device_ll_client
    prov_device_client
    prov_auth_client)

  message(STATUS "Added compile definition, libraries and include directories for the provisioning module.")
endif()

list(APPEND AzureIoT_LIBRARIES
  aziotsharedutil)

add_library(azure_iot_sdks INTERFACE)
target_include_directories(azure_iot_sdks INTERFACE ${AzureIoT_INCLUDE_DIRS})
target_link_libraries(azure_iot_sdks INTERFACE ${AzureIoT_LIBRARIES})

list(POP_BACK CMAKE_MESSAGE_INDENT)
