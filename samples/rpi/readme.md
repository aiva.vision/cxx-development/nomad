# Raspberry Pi

This sample can send telemetry, process commands from IoT Central/Hub, process twin updates and report properties.

Telemetry:
- used memory
- used swap
- free storage

Methods:
- update location

Persistent properties:
- location

Read-only properties:
- Nomad version
- cpu name
- cpu architecture
- total storage
- total memory
- total swap
