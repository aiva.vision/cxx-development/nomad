#define DOCTEST_CONFIG_IMPLEMENT
#include <doctest/doctest.h>
#include <spdlog/spdlog.h>

#include "rpi/rpi.hxx"

void stop(int signal_number) {

  spdlog::info("Caught signal with ID {}, stopping ...", signal_number);
  RPi::stop();
}

int main(int argc, char *argv[]) {

  doctest::Context context;
  context.setOption("exit", false);
  context.applyCommandLine(argc, argv);

  int res = context.run();
  if (context.shouldExit()) {
    return res; // for the query flags
  }
  /*
     Register signal handlers to exit gracefully.
     */
  signal(SIGINT, stop);
  /*
     Start monitoring.
     */
  RPi{}.start();

  spdlog::info("Exited gracefully.");
  return 0;
}
