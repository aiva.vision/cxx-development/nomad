#include <nomad/blob_storage.hxx>
#include <nomad/exceptions.hxx>
#include <spdlog/spdlog.h>

#include "rpi/methods.hxx"
#include "rpi/rpi.hxx"

Update_Property_Method::Update_Property_Method(const std::string &method_name,
                                               const std::string &property)
    : nomad::Method{method_name}, property_name{property} {}

nomad::Status Update_Property_Method::execute(const nlohmann::json &payload,
                                              nomad::Method_Response &response,
                                              nomad::Component *component) {

  response.set(nomad::STATUS_INTERNAL_ERROR);

  if (!payload.is_string()) {
    response.set(nomad::STATUS_BAD_FORMAT);
    throw nomad::Method_Processing_Error(get_name(), "Payload is not a string",
                                         nomad::STATUS_BAD_FORMAT);
  }
  component->set_persistent_property(property_name, payload);
  component->save_persistent_properties(RPi::properties_location);
  component->send_persistent_property(property_name);

  response.set(nomad::STATUS_SUCCESS);
  spdlog::info("Processed the {} method successfully.", get_name());

  return nomad::STATUS_SUCCESS;
}

Update_Location_Method::Update_Location_Method()
    : Update_Property_Method("update_location", "location") {}
