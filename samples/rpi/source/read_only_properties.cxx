#include <doctest/doctest.h>
#include <trompeloeil.hpp>

#include <nomad/system.hxx>
#include <nomad/version.hxx>
#include <string>
#include <thread>

#include "rpi/read_only_properties.hxx"

class Mock_IoT_Hub_Client
    : public trompeloeil::mock_interface<nomad::IoT_Hub_Client_Adapter> {
  IMPLEMENT_MOCK1(set_method_controller);
  IMPLEMENT_MOCK1(set_twin_controller);
  IMPLEMENT_CONST_MOCK1(send_message);
  IMPLEMENT_CONST_MOCK1(send_property, noexcept);
  IMPLEMENT_MOCK0(do_work, noexcept);
};

Nomad_Version_Property::Nomad_Version_Property(
    const std::shared_ptr<nomad::IoT_Hub_Client_Adapter> &client)
    : nomad::Read_Only_Property("nomad_version", client) {}

nlohmann::json Nomad_Version_Property::get_property_content() const {
  return nomad::get_project_version();
}

CPU_Name_Property::CPU_Name_Property(
    const std::shared_ptr<nomad::IoT_Hub_Client_Adapter> &client)
    : Read_Only_Property("cpu_name", client) {}

nlohmann::json CPU_Name_Property::get_property_content() const {

  return extract_cpu_name(
      nomad::System::exec_shell(R"(lscpu | grep 'Model name')"));
}

std::string CPU_Name_Property::extract_cpu_name(const std::string &info) const {

  auto cpu_name = info.substr(info.find(':') + 1);
  cpu_name = cpu_name.substr(cpu_name.find_first_not_of(' '));

  return cpu_name;
}

SCENARIO("Extracting the CPU name from the lscpu output.") {

  GIVEN("A CPU name property agent and the lscpu info.") {

    auto client = std::make_shared<Mock_IoT_Hub_Client>();
    ALLOW_CALL(*client, send_property(ANY(std::string &))).RETURN(true);
    ALLOW_CALL(*client, send_message(ANY(const nomad::Message &)));

    CPU_Name_Property prop{client};
    auto lscpu_info_1 = "Model name:                      AMD Ryzen 7 5800H "
                        "with Radeon Graphics";
    auto lscpu_info_2 =
        "Model name:                      AMD Ryzen 5 2600 Six-Core Processor";

    WHEN("The lscpu info is parsed.") {

      auto cpu_name_1 = prop.extract_cpu_name(lscpu_info_1);
      auto cpu_name_2 = prop.extract_cpu_name(lscpu_info_2);

      THEN("Everything should be removed except the model name.") {

        CHECK_EQ(cpu_name_1, "AMD Ryzen 7 5800H with Radeon Graphics");
        CHECK_EQ(cpu_name_2, "AMD Ryzen 5 2600 Six-Core Processor");
      }
    }
  }
}

CPU_Architecture_Property::CPU_Architecture_Property(
    const std::shared_ptr<nomad::IoT_Hub_Client_Adapter> &client)
    : Read_Only_Property("cpu_architecture", client) {}

nlohmann::json CPU_Architecture_Property::get_property_content() const {
  return nomad::System::exec_shell(
      R"(lscpu | grep Architecture | awk '{printf "%s", $2}' 2>&1)");
}

Total_Storage_Property::Total_Storage_Property(
    const std::shared_ptr<nomad::IoT_Hub_Client_Adapter> &client)
    : Read_Only_Property("total_storage", client) {}

nlohmann::json Total_Storage_Property::get_property_content() const {
  return std::stoul(nomad::System::exec_shell(
      R"(df -m / | awk 'FNR == 2 {printf "%s", $2}' 2>&1)"));
}

Total_Memory_Property::Total_Memory_Property(
    const std::shared_ptr<nomad::IoT_Hub_Client_Adapter> &client)
    : Read_Only_Property("total_memory", client) {}

nlohmann::json Total_Memory_Property::get_property_content() const {
  return std::stoul(nomad::System::exec_shell(
      R"(free -m | grep Mem: | awk '{printf "%s", $2}' 2>&1)"));
}

Total_Swap_Property::Total_Swap_Property(
    const std::shared_ptr<nomad::IoT_Hub_Client_Adapter> &client)
    : Read_Only_Property("total_swap", client) {}

nlohmann::json Total_Swap_Property::get_property_content() const {
  return std::stoul(nomad::System::exec_shell(
      R"(free -m | grep Swap: | awk '{printf "%s", $2}' 2>&1)"));
}
