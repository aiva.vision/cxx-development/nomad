#include <nomad/exceptions.hxx>
#ifndef HAVE_BLOB_STORAGE
#error "Nomad was not compiled with Blob Storage support."
#endif // HAVE_BLOB_STORAGE

#include <nomad/blob_storage.hxx>
#include <nomad/iot_hub_client_adapter.hxx>
#include <nomad/method_controller.hxx>
#include <nomad/network.hxx>
#include <nomad/system.hxx>
#include <nomad/twin_controller.hxx>
#include <spdlog/spdlog.h>
#include <thread>

#include "rpi/methods.hxx"
#include "rpi/read_only_properties.hxx"
#include "rpi/rpi.hxx"
#include "rpi/telemetry.hxx"

using namespace std::chrono_literals;

const char *Persistent_Properties::location = "location";

const std::filesystem::path RPi::properties_location{"/etc/rpi"};
const std::filesystem::path RPi::saved_data_location{"/var/lib/rpi/data"};

const unsigned RPi::max_disk_usage = 95;

/*
   Initialize default values.
   */
bool RPi::running = true;
/*
   Calling do_work() this often is required for sending heartbeats.
   */
std::chrono::milliseconds RPi::do_work_interval{100};
/*
   Send telemetry every 3 minutes.
   */
unsigned RPi::send_telemetry_interval{1800};
/*
   Uploading data frequently is desired.
   */
std::chrono::seconds RPi::data_upload_interval{1};

bool RPi::is_running() noexcept { return running; }

void RPi::stop() noexcept { running = false; }

RPi::RPi()
    : client{nomad::create_iot_hub_client()},
      model{std::make_shared<nomad::CModel>(client)}, twin_controller{model},
      method_controller{model} {

  client->set_twin_controller(&twin_controller);
  client->set_method_controller(&method_controller);

  spdlog::info("Created the IoT Hub/Central client and the twin and method "
               "controllers.");

  model->acquire_component(build_default_component());

  spdlog::info("Built and acquired the default component.");

  upload_status =
      std::async(std::launch::async, &RPi::check_for_saved_data, this);
}

RPi::~RPi() {
  model->do_work();
  model->save_persistent_properties(properties_location);
}

std::unique_ptr<nomad::Component> RPi::build_default_component() {

  auto component = std::make_unique<nomad::CComponent>(client);
  /*
     Build telemetry.
     */
  component->acquire_telemetry(std::make_unique<Used_Memory_Telemetry>(client));
  component->acquire_telemetry(std::make_unique<Used_Swap_Telemetry>(client));
  component->acquire_telemetry(
      std::make_unique<Free_Storage_Telemetry>(client));
  /*
     Build read only properties.
     */
  component->acquire_read_only_property(
      std::make_unique<Nomad_Version_Property>(client));
  component->acquire_read_only_property(
      std::make_unique<CPU_Name_Property>(client));

  component->acquire_read_only_property(
      std::make_unique<CPU_Architecture_Property>(client));
  component->acquire_read_only_property(
      std::make_unique<Total_Storage_Property>(client));
  component->acquire_read_only_property(
      std::make_unique<Total_Memory_Property>(client));
  component->acquire_read_only_property(
      std::make_unique<Total_Swap_Property>(client));
  /*
     Build methods.
     */
  component->acquire_method(std::make_unique<Update_Location_Method>());

  return component;
}
/*
   Empties the folders if the disk usage is above the threshold.
   */
static void check_disk_usage(unsigned max_usage) {

  const auto storage = std::filesystem::space("/");
  const auto usage =
      100 - static_cast<double>(storage.available) / storage.capacity * 100;

  if (usage > max_usage) {

    for (const auto &entry :
         std::filesystem::directory_iterator(RPi::saved_data_location)) {

      nomad::System::empty_directory(entry);
      spdlog::warn("Emptied {} due to insufficient storage.",
                   entry.path().string());
    }
  }
}

void RPi::check_for_saved_data() {

  spdlog::info("Started to upload files from {} every {} seconds.",
               saved_data_location.string(), data_upload_interval.count());

  if (!std::filesystem::exists(saved_data_location)) {
    std::filesystem::create_directory(saved_data_location);
  }
  while (running) {

    if (nomad::Network::status() == nomad::Network::Status::down) {

      spdlog::warn("The network is currently down, skipping upload...");

      std::this_thread::sleep_for(3s);
      continue;
    }
    for (const auto &entry :
         std::filesystem::directory_iterator(saved_data_location)) {
      /*
         A directory represents the container from the storage account.
         */
      if (!entry.is_directory()) {
        continue;
      }
      try {
        const std::string &location =
            model->get_persistent_property(Persistent_Properties::location);

        const auto container = location + "-" + entry.path().stem().string();

        auto count = nomad::upload_files(entry, container, true, true);

        if (count) {
          spdlog::info("Uploaded {} files to the '{}' container.", count,
                       container);
        }
        model->send_message("uploaded_files", count);

      } catch (const std::out_of_range &e) {
        spdlog::error("The location hasn't been set yet, skipping upload.");

      } catch (const std::exception &e) {
        spdlog::error("Failed to upload file: {}", e.what());
      }
    }
    /*
       Delete the data before running out of storage if the uploading failed.
       */
    check_disk_usage(max_disk_usage);

    unsigned seconds_count = 0;
    while (running && seconds_count++ < data_upload_interval.count()) {
      std::this_thread::sleep_for(1s);
    }
  }
}

void RPi::start() {

  try {
    model->load_persistent_properties(properties_location);
    model->send_persistent_properties();

    spdlog::info("Loaded the persistent properties from {}.",
                 properties_location.string());

  } catch (const nomad::Invalid_Location &e) {

    spdlog::error("Failed to load the persistent properties from {}.",
                  properties_location.string());
  }
  spdlog::info("Sent the available persistent properties.");
  /*
     Send the device properties.
     */
  model->send_read_only_properties();

  spdlog::info("Sent the read only properties.");

  unsigned iteration_count = 0;
  while (running) {

    if (++iteration_count == send_telemetry_interval) {
      iteration_count = 0;

      try {
        model->send_telemetry();
        spdlog::info("Sent telemetry.");

      } catch (const std::exception &e) {
        spdlog::error("Failed to send telemetry: {}", e.what());
      }
    }
    if (upload_status.wait_for(0s) == std::future_status::ready) {

      spdlog::error("The file uploading task has crashed.");
      upload_status.get();
    }
    model->do_work();
    std::this_thread::sleep_for(do_work_interval);
  }
}
