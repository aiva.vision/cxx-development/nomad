#include <nomad/system.hxx>

#include "rpi/telemetry.hxx"

nlohmann::json Used_Memory_Telemetry::get_telemetry_data() const {

  return nlohmann::json{
      {"used_memory", std::stoul(nomad::System::exec_shell(
                          R"(free -m | awk 'NR == 2 {printf $3}' 2>&1)"))}};
}
nlohmann::json Used_Swap_Telemetry::get_telemetry_data() const {

  return nlohmann::json{
      {"used_swap", std::stoul(nomad::System::exec_shell(
                        R"(free -m | awk 'NR == 3 {printf $3}' 2>&1)"))}};
}

Free_Storage_Telemetry::Free_Storage_Telemetry(
    const std::shared_ptr<nomad::IoT_Hub_Client_Adapter> &client)
    : nomad::Basic_Telemetry(client) {}

nlohmann::json Free_Storage_Telemetry::get_telemetry_data() const {

  return nlohmann::json{
      {"free_storage", std::stoul(nomad::System::exec_shell(
                           R"(df -m / | awk 'FNR == 2 {printf "%s", $4}')"))}};
}
