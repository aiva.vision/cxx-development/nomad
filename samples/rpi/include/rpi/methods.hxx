#pragma once

#include "nomad/method.hxx"

/**
 * @brief Represents a method to update a property.
 */
class Update_Property_Method : public nomad::Method {
public:
  /**
   * @brief Constructor for the Update_Property_Method class.
   *
   * @param method_name The name of the method.
   * @param property The property to update.
   */
  Update_Property_Method(const std::string &method_name,
                         const std::string &property);

  /**
   * @brief Virtual destructor for the Update_Property_Method class.
   */
  virtual ~Update_Property_Method() {}

  /**
   * @brief Executes the method.
   *
   * @param payload The payload for the method.
   * @param response The response from the method.
   * @param component The component to execute the method on.
   * @return The status of the execution.
   */
  nomad::Status execute(const nlohmann::json &payload,
                        nomad::Method_Response &response,
                        nomad::Component *component);

private:
  /**
   * @brief The name of the property to update.
   */
  std::string property_name;
};

/**
 * @brief Represents a method to update a location.
 */
class Update_Location_Method : public Update_Property_Method {
public:
  /**
   * @brief Constructor for the Update_Location_Method class.
   */
  Update_Location_Method();
};
