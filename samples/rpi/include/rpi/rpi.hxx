#pragma once

#include <future>

#include <nomad/iot_hub_client_adapter.hxx>
#include <nomad/method_controller.hxx>
#include <nomad/model.hxx>
#include <nomad/twin_controller.hxx>

namespace Persistent_Properties {

extern const char *location;

} // namespace Persistent_Properties

/**
 * @brief Facade for Nomad's model and components.
 */
class RPi {
public:
   /**
    * @brief Location where persistent properties are saved.
    */
   static const std::filesystem::path properties_location;

   /**
    * @brief Location where data can be saved and uploaded to Blob Storage.
    */
   static const std::filesystem::path saved_data_location;

   /**
    * @brief Maximum amount of data that can be saved before deleting it.
    */
   static const unsigned max_disk_usage;

   /**
    * @brief Queries if RPi4 is still running.
    * 
    * @return True if RPi4 is running, false otherwise.
    */
   static bool is_running() noexcept;

   /**
    * @brief Sets the running flag to false.
    */
   static void stop() noexcept;

   /**
    * @brief Builds the client, the model and the controllers etc.
    */
   RPi();

   /**
    * @brief Syncs telemetry and properties and saves persistent properties.
    */
   ~RPi();

   /**
    * @brief Starts a loop based on nomad::should_exit.
    */
   void start();

private:
   /**
    * @brief Holds the state of the app.
    */
   static bool running;

   /**
    * @brief Interval between calls to do_work().
    */
   static std::chrono::milliseconds do_work_interval;

   /**
    * @brief Every time the main loop wakes up, on the send_telemetry_interval(th) pass will send telemetry messages. 
    * So we will send telemetry every (send_telemetry_interval * do_work_interval) milliseconds.
    */
   static unsigned send_telemetry_interval;

   /**
    * @brief Time to sleep between scans for new files.
    */
   static std::chrono::seconds data_upload_interval;

   /**
    * @brief Builds the default component.
    * 
    * @return A unique pointer to the default component.
    */
   std::unique_ptr<nomad::Component> build_default_component();

   /**
    * @brief Checks for new saved data and uploads them.
    */
   void check_for_saved_data();

   /**
    * @brief Checks if the modules are not and restarts the failed ones.
    */
   void check_modules();

   /**
    * @brief Client for Iot Hub/Central communication.
    */
   std::shared_ptr<nomad::IoT_Hub_Client_Adapter> client;

   /**
    * @brief Holds the state of the app.
    */
   std::shared_ptr<nomad::Model> model;

   /**
    * @brief Processes twin updates.
    */
   nomad::Twin_Controller twin_controller;

   /**
    * @brief Processes method updates.
    */
   nomad::Method_Controller method_controller;

   /**
    * @brief Handle to long running method for saved data.
    */
   std::future<void> upload_status;
};
