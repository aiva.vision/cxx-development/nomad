#pragma once

#include "nomad/property.hxx"

/**
 * @brief Represents a read-only property for the Nomad version.
 */
class Nomad_Version_Property : public nomad::Read_Only_Property {
public:
  /**
   * @brief Constructor for the Nomad_Version_Property class.
   * 
   * @param client Shared pointer to the IoT_Hub_Client_Adapter.
   */
  Nomad_Version_Property(
      const std::shared_ptr<nomad::IoT_Hub_Client_Adapter> &client);

  /**
   * @brief Retrieves the content of the property.
   * 
   * @return The content of the property.
   */
  nlohmann::json get_property_content() const;
};

/**
 * @brief Represents a read-only property for the CPU name.
 */
class CPU_Name_Property : public nomad::Read_Only_Property {
public:
  /**
   * @brief Constructor for the CPU_Name_Property class.
   * 
   * @param client Shared pointer to the IoT_Hub_Client_Adapter.
   */
  CPU_Name_Property(
      const std::shared_ptr<nomad::IoT_Hub_Client_Adapter> &client);

  /**
   * @brief Retrieves the content of the property.
   * 
   * @return The content of the property.
   */
  nlohmann::json get_property_content() const;

  /**
   * @brief Extracts the CPU name from the provided info.
   * 
   * @param info The info to extract the CPU name from.
   * @return The CPU name.
   */
  std::string extract_cpu_name(const std::string &info) const;
};

/**
 * @brief Represents a read-only property for the CPU architecture.
 */
class CPU_Architecture_Property : public nomad::Read_Only_Property {
public:
  /**
   * @brief Constructor for the CPU_Architecture_Property class.
   * 
   * @param client Shared pointer to the IoT_Hub_Client_Adapter.
   */
  CPU_Architecture_Property(
      const std::shared_ptr<nomad::IoT_Hub_Client_Adapter> &client);

  /**
   * @brief Retrieves the content of the property.
   * 
   * @return The content of the property.
   */
  nlohmann::json get_property_content() const;
};

/**
 * @brief Represents a read-only property for the total storage.
 */
class Total_Storage_Property : public nomad::Read_Only_Property {
public:
  /**
   * @brief Constructor for the Total_Storage_Property class.
   * 
   * @param client Shared pointer to the IoT_Hub_Client_Adapter.
   */
  Total_Storage_Property(
      const std::shared_ptr<nomad::IoT_Hub_Client_Adapter> &client);

  /**
   * @brief Retrieves the content of the property.
   * 
   * @return The content of the property.
   */
  nlohmann::json get_property_content() const;
};

/**
 * @brief Represents a read-only property for the total memory.
 */
class Total_Memory_Property : public nomad::Read_Only_Property {
public:
  /**
   * @brief Constructor for the Total_Memory_Property class.
   * 
   * @param client Shared pointer to the IoT_Hub_Client_Adapter.
   */
  Total_Memory_Property(
      const std::shared_ptr<nomad::IoT_Hub_Client_Adapter> &client);

  /**
   * @brief Retrieves the content of the property.
   * 
   * @return The content of the property.
   */
  nlohmann::json get_property_content() const;
};

/**
 * @brief Represents a read-only property for the total swap.
 */
class Total_Swap_Property : public nomad::Read_Only_Property {
public:
  /**
   * @brief Constructor for the Total_Swap_Property class.
   * 
   * @param client Shared pointer to the IoT_Hub_Client_Adapter.
   */
  Total_Swap_Property(
      const std::shared_ptr<nomad::IoT_Hub_Client_Adapter> &client);

  /**
   * @brief Retrieves the content of the property.
   * 
   * @return The content of the property.
   */
  nlohmann::json get_property_content() const;
};
