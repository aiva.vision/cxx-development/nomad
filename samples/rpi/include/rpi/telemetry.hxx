#pragma once

#include <nomad/telemetry.hxx>

/**
 * @brief Calls the free command and parses the output to build the used_memory
 * telemetry.
 */
class Used_Memory_Telemetry : public nomad::Basic_Telemetry {
public:
  using nomad::Basic_Telemetry::Basic_Telemetry;

  /**
   * @brief Retrieves the telemetry data.
   *
   * @return The telemetry data.
   */
  nlohmann::json get_telemetry_data() const;
};

/**
 * @brief Calls the free command and parses the output to build the used_swap
 * telemetry.
 */
class Used_Swap_Telemetry : public nomad::Basic_Telemetry {
public:
  using nomad::Basic_Telemetry::Basic_Telemetry;

  /**
   * @brief Retrieves the telemetry data.
   *
   * @return The telemetry data.
   */
  nlohmann::json get_telemetry_data() const;
};

/**
 * @brief Calls the free command and parses the output to build the free_storage
 * telemetry.
 */
class Free_Storage_Telemetry : public nomad::Basic_Telemetry {
public:
  /**
   * @brief Constructor for the Free_Storage_Telemetry class.
   *
   * @param client Shared pointer to the IoT_Hub_Client_Adapter.
   */
  Free_Storage_Telemetry(
      const std::shared_ptr<nomad::IoT_Hub_Client_Adapter> &client);

  /**
   * @brief Retrieves the telemetry data.
   *
   * @return The telemetry data.
   */
  nlohmann::json get_telemetry_data() const;
};
