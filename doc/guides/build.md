# Build

1. Install dependecies

- [CMake](https://cmake.org/install/)
- [nlohman_json](https://github.com/nlohmann/json)
- [Libxml2](http://www.xmlsoft.org/)
- [curl](https://curl.se/)
- [OpenSSL](https://www.openssl.org/)
- [Doctest](https://github.com/onqtam/doctest)
- [Trompeloeil](https://github.com/rollbear/trompeloeil)

Development:

```zsh
sudo apt update && sudo apt install -y \
  libcurl4-openssl-dev \
  libssl-dev \
  libxml2-dev \
  uuid-dev
```

Runtime:

```zsh
sudo apt update && sudo apt install -y \
   libssl1.1 \
   libcurl4 \
   libxml2 \
   uuid-runtime
```

2. Clone this repo.

```zsh
git clone --recursive git@gitlab.com:aiva.vision/cxx-development/nomad.git
```

3. Configure and build.

```zsh
cmake -S nomad -B nomad/debug \
  -D CMAKE_BUILD_TYPE=Debug \
  -D build_tests=ON \
  -D use_blob_storage=ON \
  -D CMAKE_EXPORT_COMPILE_COMMANDS=ON

cmake --build nomad/debug -j
```

4. Optionally install.

```zsh
cmake --install nomad/debug
```
