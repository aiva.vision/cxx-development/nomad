### Environment setup

Always set the connection type:

- `IOTHUB_CONNECTION_TYPE`, can be `EDGE`, `DPS` or `DCS`

For an `DPS` connection type, set the following:

- `IOTHUB_DEVICE_DPS_ID_SCOPE`
- `IOTHUB_DEVICE_DPS_DEVICE_ID`
- `IOTHUB_DEVICE_DPS_DEVICE_KEY`
- `IOTHUB_DEVICE_TEMPLATE_ID`

For a `DCS` connection type, set the following:

- `IOTHUB_DEVICE_CONNECTION_STRING`
- `IOTHUB_DEVICE_TEMPLATE_ID`

For uploading and downloading files to/from Blob Storage, set the connection string:

- `AZURE_STORAGE_CONNECTION_STRING`

Optional environment variables:

- `IOTHUB_DEVICE_DPS_ENDPOINT`, defaults to `global.azure-devices-provisioning.net`
- `IOTHUB_ENABLE_TRACING`, should hold `true` or `false`, defaults to `false`
