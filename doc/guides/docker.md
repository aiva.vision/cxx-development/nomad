# Nomad base images

Install QEMU to build ARM images on AMD64.

```zsh
sudo apt-get update && sudo apt-get install -y \
  qemu qemu-system-misc \
  qemu-user-static qemu-user \
  binfmt-support
```

Use `build_images.sh` to build docker images.

```zsh
./docker/build_images.sh
Usage: build_images.sh [:hv:g:u:a:]

    -h      print this
    -v      nomad_version
    -g      gcc_version
    -u      ubuntu_version, codename or release
    -a      architecture, arm64v8 or amd64
```
