# Documentation

This project uses [Doxygen](https://www.doxygen.nl/index.html) to generate documentation.

1. Generate documentation.

```sh
cd docs
doxygen Doxyfile
```

2. Open the documentation in your browser.

Depending on how you installed the browser, you may not be able to open the file from the command line.
Flatpak and Snap generally restrict the access to the filesystem.

```sh
firefox docs/html/index.html
```
