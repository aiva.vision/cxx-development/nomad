# Nomad

**AiVA IoT Plug and Play**

![Class diagram](doc/diagrams/class_diagram.png 'Class diagram')

This library provides a higher level universal API for a any IoT Central/Hub/Edge deployment containing [device gateways](https://docs.microsoft.com/en-us/azure/iot-edge/iot-edge-as-gateway?view=iotedge-2018-06), [components](https://docs.microsoft.com/en-us/azure/iot-central/core/concepts-device-templates) and [modules](https://docs.microsoft.com/en-us/azure/iot-edge/iot-edge-modules?view=iotedge-2018-06). Nomad leverages the [Composite](https://en.wikipedia.org/wiki/Composite_pattern) pattern to unify the low level components of an IoT deployment. The properties, methods and telemetry are instantiated and acquired by components, which are later acquired by the model. The provisioning happens exclusively through the environment and the resulting application can be deployed using any of the supported provisioning options without implementing any provisioning logic at all. The communication with IoT Hub/Central, the payload serialization/deserialization and the incoming methods or property updates processing are abstracted away. Therefore, the only aspects left for the developer are the functionality and hardware specific telemetry data extraction.

Persistent properties have been introduced as a replacement for the writable properties, which are unavailable for modules. The properties are serialized to disk to ensure persistence even in case of power loss.

Available guides:

- [Build](doc/guides/build.md)
- [Environment](doc/guides/environment.md)
- [Docker](doc/guides/docker.md)
